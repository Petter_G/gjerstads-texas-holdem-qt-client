#! /bin/bash

local="$HOME/Dropbox/GHoldemClientCPP"

klientHcount=$(find "$local" -maxdepth 1 -name '*.h' | grep -v 'moc' | grep -v 'qrc'| xargs wc -l | awk 'NR==0; END{print}'| awk '{print $1}')
klientCPPcount=$(find "$local" -maxdepth 1 -name '*.cpp' | grep -v 'moc'| grep -v 'qrc'| xargs wc -l | awk 'NR==0; END{print}'| awk '{print $1}')

sum=$(( klientHcount + klientCPPcount ))

#echo "Klient .h count: $klientHcount"
#echo "klient .cpp count: $klientCPPcount"

klientSum=$(( klientHcount + klientCPPcount ))
echo "Combined Klient lines: $klientSum"
