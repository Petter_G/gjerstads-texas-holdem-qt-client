#include "winnerdata.h"

#include <stdexcept>

WinnerData::WinnerData(std::vector<int>& data) : winnerTableslot(data[1]),payout(data[2]),potNumber(data[3])
{
    if (data.size() != 4)
        throw std::out_of_range("Size: "+std::to_string(data.size()) );
}

int WinnerData::getWinner()
{
    return winnerTableslot;
}

int WinnerData::getPayout()
{
    return payout;
}

int WinnerData::getPotNumber()
{
    return potNumber;
}
