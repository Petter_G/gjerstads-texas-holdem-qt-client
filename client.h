#ifndef CLIENT_H
#define CLIENT_H

//#include "uipainter.h"
#include "uipainter.h"
#include "holdemgui.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <arpa/inet.h>
#include <string.h>
#include <thread>
#include <memory>

#include <QWidget>
#include <QMainWindow>
#include <QDebug>

typedef unsigned int uint;

class UIpainter;
class HoldemGUI;

class Client
{
    std::unique_ptr<UIpainter> UI;

public:
    Client(std::string SERVERLISTENPORT, std::string SERVERIP, HoldemGUI *gui);
private:
    void incThread(uint fd);
    void outThread(unsigned int fd);
    void error(const char *msg);
    void* get_in_addr(sockaddr *sa);
    bool inputCheck();
};

#endif // CLIENT_H
