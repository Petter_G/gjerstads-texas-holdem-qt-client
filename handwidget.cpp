#include "handwidget.h"

#include <QLayout>
#include <QPushButton>

#include <iostream>

handWidget::handWidget(QWidget *parent, bool orientation) : QWidget(parent)
{
    bet = new QLabel;
    bet->setAlignment(Qt::AlignRight);
    bet->setStyleSheet(
        "background: darkblue;"
        "border-style: outset;"
        "color: ghostwhite;"
        "border-color: darkblue;"
        "border-width: 3px;"
        "border-radius: 10px;");

    balance = new QLabel;
    balance->setAlignment(Qt::AlignRight);
    balance->setStyleSheet(
        "background: darkblue;"
        "border-style: outset;"
        "color: ghostwhite;"
        "border-color: darkblue;"
        "border-width: 3px;"
        "border-radius: 10px;");

    playername = new QLabel;
    playername->setAlignment(Qt::AlignLeft);
    playername->setStyleSheet(
        "background: darkblue;"
        "color: ghostwhite;"
        "border-style: outset;"
        "border-color: darkblue;"
        "border-width: 5px;"
        "border-radius: 10px;");

    leftCard = new QLabel;
    leftCard->setFixedSize(71,96);
    rightCard = new QLabel;
    rightCard->setFixedSize(71,96);

    button = new QLabel;
    YOULabel = new QLabel;
    YOULabel->setPixmap(QPixmap(tr(":/images/transparantYOUlabel.png")) );
    YOULabel->sizePolicy().setRetainSizeWhenHidden(true);
    YOULabel->hide();

    QHBoxLayout* cardLayout = new QHBoxLayout;

    QHBoxLayout* buttonLayout = new QHBoxLayout;

    cardLayout->addStretch();
    cardLayout->addWidget(leftCard);
    QSpacerItem* spacer = new QSpacerItem(10,0,QSizePolicy::Fixed,QSizePolicy::Fixed);
    cardLayout->addSpacerItem(spacer);
    cardLayout->addWidget(rightCard);
    cardLayout->addStretch();

    buttonLayout->addWidget(YOULabel);
    buttonLayout->addStretch();
    buttonLayout->addWidget(button);

    QVBoxLayout* outerLayout = new QVBoxLayout;
    QVBoxLayout* fieldsLayout = new QVBoxLayout;

    if (orientation )//aka upper
    {
        fieldsLayout->addWidget(playername);
        fieldsLayout->addWidget(balance);
        fieldsLayout->addWidget(bet);
        outerLayout->addLayout(fieldsLayout);

        outerLayout->addLayout(cardLayout);
        outerLayout->addLayout(buttonLayout);
    }
    else
    {
        outerLayout->addLayout(buttonLayout);
        outerLayout->addLayout(cardLayout);

        fieldsLayout->addWidget(playername);
        fieldsLayout->addWidget(balance);
        fieldsLayout->addWidget(bet);
        outerLayout->addLayout(fieldsLayout);
    }

    //begrenser mellomrommet mellom objektene.
    fieldsLayout->setSpacing(2);

    outerLayout->setSizeConstraint(QLayout::SetFixedSize);
    setLayout(outerLayout);

    hide();
}

handWidget::~handWidget()
{

}

void handWidget::setPlayername(QString name)
{
    playername->setText( name );
}

void handWidget::setGFXleftCard(short type,short value)
{
    if (type < 0 || type > 3)
    {
        std::cout<<"error in set GFXleftCard: type corrupt: "<<type<<std::endl;
        std::abort();
    }
    else if (value < 1 || value > 13)
    {
        std::cout<<"error in set GFXleftCard: value corrupt: "<<value<<std::endl;
        std::abort();
    }

    QString imageURL=resourceBase;
    imageURL.append(QString::fromStdString(std::to_string(type)) );

    std::string value_text;
    if (value < 10)
        value_text="0" +std::to_string(value);
    else
        value_text=std::to_string(value);

    imageURL.append(QString::fromStdString(value_text) );
    imageURL.append(".gif");

    leftCard->setPixmap(QPixmap(imageURL));
}

void handWidget::setGFXrightCard(short type,short value)
{
    if (type < 0 || type > 3)
    {
        std::cout<<"error in set GFXrightCard: type corrupt: "<<type<<std::endl;
        std::abort();
    }
    else if (value < 1 || value > 13)
    {
        std::cout<<"error in set GFXrightCard: value corrupt: "<<value<<std::endl;
        std::abort();
    }

    QString imageURL=resourceBase;
    imageURL.append(QString::fromStdString(std::to_string(type)) );

    std::string value_text;
    if (value < 10)
        value_text="0" +std::to_string(value);
    else
        value_text=std::to_string(value);

    imageURL.append(QString::fromStdString(value_text) );
    imageURL.append(".gif");

    rightCard->setPixmap(QPixmap(imageURL));
}

void handWidget::showBackside()
{
    QString imageURL = resourceBase;
    imageURL.append("xx.png");
    leftCard->setPixmap(imageURL);
    rightCard->setPixmap(imageURL);
}

void handWidget::hideCards()
{
    leftCard->setPixmap(tr(":/images/blank.png") );
    rightCard->setPixmap(tr(":/images/blank.png") );
}

void handWidget::showCards()
{
    leftCard->show();
    rightCard->show();
}

void handWidget::setBet(uint betvalue, uint totalbet)
{
    QString text = "Bet: $";
    text.append(QString::number(betvalue));
    text.append(tr("("));
    text.append(QString::number(totalbet));
    text.append(tr(")"));
    bet->setText(text);
}

void handWidget::markBetFolded() //true=fold,false=allin
{
    bet->setText(tr("Folded"));
}

void handWidget::markNameLeaving()
{
    QString current = playername->text();
    current.append(tr(" (leaving)"));
    playername->setText(current);
}

void handWidget::markBetAllin()
{
    bet->setText(tr("Allin"));
}

void handWidget::setBalance(uint value)
{
    QString text = "Balance: $";
    text.append(QString::number(value));
    balance->setText(text);
    //balance->setText(QString::fromStdString(std::to_string(value)) );
}

void handWidget::toggleDealerButton(bool state)
{
    QString imageURL = resourceBase;
    if (state)
    {
        imageURL.append("transparantDealerButton2.png");
        button->setPixmap(imageURL);
    }
    else
    {
        button->clear();
        imageURL.append("BLANKtransparantDealerButton2.png");
        button->setPixmap(imageURL);
    }
}

void handWidget::toggleYOULabel(bool state)
{
    if (state)
    {
        YOULabel->show();
    }
    else
    {
        YOULabel->hide();
    }
}

void handWidget::toggleOpaque(bool state)
{

    //effects
    opacityONleft = new QGraphicsOpacityEffect();
    opacityONright = new QGraphicsOpacityEffect();
    opacityONleft->setOpacity(0.75);
    opacityONright->setOpacity(0.75);
    if (state)
    {
        leftCard->setGraphicsEffect(opacityONleft );
        rightCard->setGraphicsEffect(opacityONright );
    }
    else
    {
        leftCard->setGraphicsEffect(nullptr );
        rightCard->setGraphicsEffect(nullptr );
    }
}
