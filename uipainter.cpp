#include "uipainter.h"

#include <exception>
#include <sstream>
#include <mutex>

using namespace std;

UIpainter::UIpainter()
{
    pot = 0;

    currentPlayerTableNr = -1;
    currentButtonTableNr = -1;
    ClientTableNr = -1;
}

void UIpainter::resetObject()
{
    communityHand.clear();
    pot = 0;

    currentPlayerTableNr = -1;
    currentButtonTableNr = -1;
    ClientTableNr = -1;
    Reps.clear();
    WinnerInfo.clear();
}

bool UIpainter::createRep(short tableslot)
{
    bool first = false;
    if (Reps.size() == 0)
    {
        setLocalTableNr(tableslot);
        first=true;
    }

    Reps.push_back(playerRepresentationClient(tableslot) );

    return first;
}

void UIpainter::setHand(vector<int> dataparcel)
{
    for(auto& player : Reps)
    {
        if (player.getTableNr() == dataparcel[1])
        {
            player.dumpCards();
            player.addCard(card(dataparcel[2],dataparcel[3] ) );
            player.addCard(card(dataparcel[4],dataparcel[5] ) );
            return;
        }
    }
    cout<<"Fatal Error: setHand failed to find tableslot("<<dataparcel[1]<<") in local repository."<<endl;
}

void UIpainter::setHand(vecUint dataparcel)
{
    for(auto& player : Reps)
    {
        if (player.getTableNr() == dataparcel[0])
        {
            player.dumpCards();
            player.addCard(card(dataparcel[1],dataparcel[2] ) );
            player.addCard(card(dataparcel[3],dataparcel[4] ) );
            return;
        }
    }
    cout<<"Fatal Error: setHand failed to find tableslot("<<dataparcel[0]<<") in local repository."<<endl;
}

void UIpainter::updateRepName(QString newname,short tableslot)
{
    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot )
        {
            player.setName(newname.toStdString());
            return;
        }
    }
    cout<<"Fatal Error: updateRepName failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}

void UIpainter::updateBet(short tableslot,int bet,int totalbet)
{

    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot )
        {
            player.setBet(bet);
            player.setTotalBet(totalbet);
            return;
        }
    }
    cout<<"Fatal Error: updateBet failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}

void UIpainter::updateBalance(int balance,short tableslot)
{

    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot)
        {
            player.setBalance(balance);
            return;
        }
    }
    cout<<"Fatal Error: updateBalance failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}
void UIpainter::updateActive(bool active,short tableslot)
{
    cout<<"UpdateActive called, bool active is: "<<active<<"short tableslot is: "<<tableslot<<endl;
    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot)
        {
            player.setActiveState(active);
            return;
        }
    }
    cout<<"Fatal Error: updateActive failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}

void UIpainter::updateReady(bool ready,short tableslot)
{

    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot)
        {
            player.setReadyState(ready);
            return;
        }
    }
    cout<<"Fatal Error: updateReady failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}

void UIpainter::updateAllIn(short tableslot)
{
    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot)
        {
            player.setAllin(true);
            return;
        }
    }
    cout<<"Fatal Error: setAllIn failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();
}


string UIpainter::getPlayerName(short tableslot)
{
    for (auto& player : Reps)
    {
        if (player.getTableNr() == tableslot)
        {
            return player.getName();
        }
    }
    cout<<"Fatal Error: getPlayerName failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();
}

void UIpainter::setLocalTableNr(short tablenr)
{
    ClientTableNr=tablenr;
}

short UIpainter::getLocalTableNr()
{
    return ClientTableNr;
}

/*int UIpainter::getRepsSize()
{
    return Reps.size();
}*/

int UIpainter::getWinnerInfoSize()
{
    return WinnerInfo.size();
}

short UIpainter::getCurrentPlayerTableNr()
{
    return currentPlayerTableNr;
}

std::vector<playerRepresentationClient>& UIpainter::getReps()
{
    return Reps;
}

void UIpainter::deleteRep(short tableslot)
{

    for (uint i = 0; i< Reps.size();i++)
    {
        if (Reps.at(i).getTableNr() == tableslot)
        {
            Reps.erase(Reps.begin()+i);
            return;
        }
    }
    cout<<"Fatal Error: deleteRep failed to find tableslot("<<tableslot<<") in local repository."<<endl;
    abort();

}

void UIpainter::resetTable()
{
    for(auto& player : Reps)
    {
        player.setBet(0);
        player.setTotalBet(0);
        player.setAllin(false);
        player.dumpCards();
    }
    communityHand.clear();
}

void UIpainter::addCardToCommunity(short type,short value)
{
    communityHand.push_back(card(type,value) );
}

void UIpainter::updatePot(short potupdate)
{
    pot = potupdate;
}

void UIpainter::updateCurrentPlayer(short currentPlayer_Tablenr)
{
    currentPlayerTableNr=currentPlayer_Tablenr;
}

void UIpainter::updateCurrentButton(short currentButton_Tablenr)
{
    currentButtonTableNr=currentButton_Tablenr;
}

void UIpainter::addWinnerData(std::vector<int>& data)
{
    try
    {
        WinnerInfo.push_back(WinnerData(data) );
    }
    catch (std::out_of_range& eRange)
    {
        cout<<"Error: Received winnerData is the wrong size(corrupt), data dropped. "<<eRange.what()<<endl;
    }
}

void UIpainter::printAndWipeWinnerData()
{
    std::map<short,int> potCounts;      //potnumber,count

    //Vi trenger å rekonstruere data om potene, heldigvis trenger vi ikke mer enn antallet vinnere per pot.
    for(auto& winner : WinnerInfo)
    {
        short potNumber = winner.getPotNumber();
        potCounts[potNumber]++;
    }

    //Nå kan vi skrive ut hver enkelt spillers gevinster.
    for(auto& winner : WinnerInfo)
    {
        cout<<SU();
        uint potnumber = winner.getPotNumber();
        uint payout = winner.getPayout();
        switch(potnumber )
        {
            case 0://mainpot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                cout<<name<<" wins "<<payout<<" from the mainpot."<<split<<endl;
                break;
            }
            case 1://first sidepot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                cout<<name<<" wins "<<payout<<" from the 1st sidepot."<<endl;
                break;
            }
            case 2://second sidepot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                cout<<name<<" wins wins "<<payout<<" from the 2nd sidepot."<<endl;
                break;
            }
            case 3://third
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                cout<<name<<" wins "<<payout<<" from the 3rd sidepot."<<endl;
                break;
            }
            default://n-th
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name =getPlayerName(winner.getWinner() );
                cout<<name<<" wins "<<payout<<" from the "<<to_string(potnumber)<<"th sidepot"<<endl;
                break;
            }
        }
    }
    WinnerInfo.clear();
}

std::string UIpainter::returnAndWipeWinnerData()
{
    using namespace std;
    map<short,int> potCounts;      //potnumber,count
    stringstream stream;

    //Vi trenger å rekonstruere data om potene, heldigvis trenger vi ikke mer enn antallet vinnere per pot.
    for(auto& winner : WinnerInfo)
    {
        short potNumber = winner.getPotNumber();
        potCounts[potNumber]++;
    }

    //Nå kan vi skrive ut hver enkelt spillers gevinster.
    for(auto& winner : WinnerInfo)
    {
        uint potnumber = winner.getPotNumber();
        uint payout = winner.getPayout();
        switch(potnumber )
        {
            case 0://mainpot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                stream<<name<<" wins "<<payout<<" from the mainpot."<<split<<endl;
                break;
            }
            case 1://first sidepot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                stream<<name<<" wins "<<payout<<" from the 1st sidepot."<<endl;
                break;
            }
            case 2://second sidepot
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                stream<<name<<" wins wins "<<payout<<" from the 2nd sidepot."<<endl;
                break;
            }
            case 3://third
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name = getPlayerName(winner.getWinner() );
                stream<<name<<" wins "<<payout<<" from the 3rd sidepot."<<endl;
                break;
            }
            default://n-th
            {
                string split = "";
                if (potCounts[potnumber] != 1 )
                    split = "(split)";

                string name =getPlayerName(winner.getWinner() );
                stream<<name<<" wins "<<payout<<" from the "<<to_string(potnumber)<<"th sidepot."<<endl;
                break;
            }
        }
    }
    WinnerInfo.clear();
    return stream.str();
}

void UIpainter::drawTable(bool exp)
{
    static int counter= 0;
    //hvis en spiller trenger hjelp, brukes disse.
    string expName= "-- Name and Tablenr -- a diamond implies currentplayer";
    string expCard= "-- Hole cards -- each player is given a pair of hole cards at the start of each game.";
    string expBalance= "-- Player Balance -- the players balance is shown here at all times";
    string expButton= "-- Button -- shows up if the player is the button, if not it's blank";
    string expBet= "-- Status and current Bet -- shows the status of the player(such as active), as well as his bet in the current round";
    string expCommunity="-- CommunityCards -- Starting in the flop, community cards show up between the two sides of the table";
    string expPot= "-- Value of the Pot -- The pot is shown here, it encompasses any and all bets the players have added to the pot";

    //Må først ha begrensninger
    short lowerbound = Reps.size()/2;
    short upperbound = Reps.size();

    //Må å sortere Reps med henhold på tablenr
    sort(Reps.begin(),Reps.end(),
        [] (playerRepresentationClient a, playerRepresentationClient b) -> bool
        {
            return a.getTableNr() < b.getTableNr();
        }
        );
    //system("clear");
    cout<<"CALLED CLEAR "<<counter++<<endl;
    //player cards lowerbound
    cout<<SU();
    for (ushort i = 0;i<lowerbound;i++) cout<<paintFText(Reps[i].getName(),Reps[i].getTableNr() );cout<<EU(expName,exp)<<"\n";
    for (ushort i = 0;i<lowerbound;i++) cout<<SU()<<paintLineAscii(false);cout<<"\n";
    for (ushort i = 0;i<lowerbound;i++) cout<<SU()<<paintUpperCardAscii(i);cout<<"\n";
    for (ushort i = 0;i<lowerbound;i++) cout<<SU()<<paintCardSpace(false);cout<<EU(expCard,exp)<<"\n";
    for (ushort i = 0;i<lowerbound;i++) cout<<SU()<<paintLowerCardAscii(i);cout<<"\n";
    for (ushort i = 0;i<lowerbound;i++) cout<<SU()<<paintLineAscii(true);cout<<"\n";

    //Balances lowerbound
    cout<<SU();
    for (ushort i = 0;i<lowerbound;i++) cout<<paintBalanceFText(i);cout<<EU(expBalance,exp)<<"\n";
    //Button lowerbound
    cout<<SU();
    for (ushort i = 0;i<lowerbound;i++) cout<<paintButtonFText(Reps[i].getTableNr() );cout<<EU(expButton,exp)<<"\n";

    //Bets lowerbound
    cout<<SU();
    for (ushort i = 0;i<lowerbound;i++) cout<<paintBetFText(i);cout<<EU(expBet,exp)<<"\n";

    //Pot
    cout<<SU()<<"\t\t"<<"Pot: <"<<pot<<">"<<EU(expPot,exp)<<"\n";

    //communitycards
    drawCommunity(expCommunity,exp);

    //Bets upperbound
    cout<<SU();
    for(ushort i = upperbound-1;i>=lowerbound;i--)cout<<paintBetFText(i);cout<<"\n";

    //Button upperbound
    cout<<SU();
    for(ushort i = upperbound-1;i>=lowerbound;i--)cout<<paintButtonFText(Reps[i].getTableNr() );cout<<"\n";

    //Balances upperbound
    cout<<SU();
    for(ushort i = upperbound-1;i>=lowerbound;i--)cout<<paintBalanceFText(i);cout<<"\n";

    //player cards upperbound
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<SU()<<paintLineAscii(false);cout<<"\n";
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<SU()<<paintUpperCardAscii(i);cout<<"\n";
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<SU()<<paintCardSpace(false);cout<<"\n";
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<SU()<<paintLowerCardAscii(i);cout<<"\n";
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<SU()<<paintLineAscii(true);cout<<"\n";
    cout<<SU();
    for(ushort i = upperbound-1;i>=lowerbound;i--) cout<<paintFText(Reps[i].getName(),Reps[i].getTableNr() );cout<<"\n";

    if (WinnerInfo.size() != 0) //Vi har data der, ergo print og flush
    {
        printAndWipeWinnerData();
    }
}

//tegner community cards.
void UIpainter::drawCommunity(string expCommunity,bool exp)
{
    cout<<SU();
    for(uint i = 0;i < communityHand.size();++i)
    {
        cout<<paintLineAscii(false,false);
    }
    cout<<"\n";
    cout<<SU();
    for (uint i = 0;i < communityHand.size();++i)
    {
        cout<<paintUpperCardAscii(communityHand,i);
    }
    cout<<"\n";
    cout<<SU();
    for (uint i = 0;i < communityHand.size();++i)
    {
        cout<<paintCardSpace();
    }
    if (exp)
        cout<<expCommunity;
    cout<<"\n";
    cout<<SU();
    for (uint i = 0;i < communityHand.size();++i)
    {
        cout<<paintLowerCardAscii(communityHand,i);
    }
    cout<<"\n";
    cout<<SU();
    for (uint i = 0;i < communityHand.size();++i)
    {
        cout<<paintLineAscii(true,false);
    }
    cout<<"\n";
}

string UIpainter::SU()  //SU="space units" likte ikke bruken av \t, ville ha en egen tabulator som returnerte hardkodede antall tabs.
{
    const short UNITS = 3;
    string space;
    for (int i= 0;i<UNITS;++i)
    {
        space.append("\t");
    }
    return space;
}

string UIpainter::EU(string var,bool explain)//Alternativ til SU, står for "explaination units". Brukes til for å lage beskrivelse av hver enkelts linje.
{
    if (explain == false)
        return "";

    string space;

    space.append(var);

    for(uint i = 0; i<var.size();i++)
    {
        space.append(" ");
    }
    return space;
}

string UIpainter::paintButtonFText(int tablenr)
{
    ushort steps = 32; //distansen mellom første kort hos to spillere på samme linje.
    string statusText = "";
    if (currentButtonTableNr == tablenr)
    {
        statusText.append( "[Button]");
    }

    auto it = find_if(Reps.begin(),Reps.end(),
                    [=](playerRepresentationClient& obj)-> bool
                    {
                        return obj.getTableNr() == tablenr;
                    });

    ushort distance = steps-statusText.size();
    for (uint i = 0; i<(distance );++i)
    {
        statusText.append(" ");
    }
    return statusText;
}

std::string UIpainter::paintBalanceFText(int index)
{
    ushort steps = 32;
    string statusText = "";

    statusText.append("[");
    string balanceText = to_string( Reps[index].getBalance());
    statusText.append(balanceText);
    statusText.append("]");

    ushort TextSize=statusText.size();

    for (ushort i = 0;i<(steps-TextSize );i++ )
    {
        statusText.append(" ");
    }
    return statusText;
}

string UIpainter::paintBetFText(int index)
{
    ushort steps = 32;
    string statusText="";

    string symbol;
    if (Reps[index].getAllin() )
        symbol = "<Allin> ";
    else if (Reps[index].getActiveState() )
        symbol= "<Active> ";
    else
        symbol="<Fold> ";

    string betText=to_string(Reps[index].getBet() );
    statusText.append(symbol);
    statusText.append("[");
    statusText.append(betText);
    statusText.append("]");

    ushort totalsize=symbol.size()+betText.size()+2;

    for (ushort i= 0;i<(steps-totalsize);i++)
    {
        statusText.append(" ");
    }
    return statusText;
}

string UIpainter::paintFText(string name,int tablenr)//FText = formatted text
{
    ushort steps = 24;   //distansen frem til spiller 2
    string distance;
    if (currentPlayerTableNr == tablenr)
    {
        distance.append("<>");
        distance.append(name);
        distance.append("["+to_string(tablenr)+"]" );
    }
    else
    {
        distance.append(name);
        distance.append("["+to_string(tablenr)+"]" );
    }

    for (uint i = 0; i<(steps-name.size());++i)
    {
        distance.append(" ");
    }
    return distance;

}

string UIpainter::paintCardSpace(bool empty)//true= bare mellomrommet uten strekene.
{
    if (empty)
    {
        return "           ";
    }
    else
        return "|   | |   |";
}

string UIpainter::paintCardSpace()//trengte visst en egen versjon for community cards.
{
    return "|   | ";
}

string UIpainter::paintLineAscii(bool linetype,bool community)//for mye overloading, community brukes ikke
{
    if ( !(linetype) )
    {
        return " ___  ";
    }
    else
    {
        const char O[] = "\xE2\x80\xBE";//O for Overscore
        string s = " ";
        s.append(O);
        s.append(O);
        s.append(O);
        s.append("  ");
        return s;
    }

}

string UIpainter::paintLineAscii(bool linetype)//true for underscore, false for overscore.
{
    const char O[] = "\xE2\x80\xBE";//O for Overscore
    if ( !(linetype) )
    {
        return " ___   ___";
    }
    else
    {
        string s = " ";
        s.append(O);
        s.append(O);
        s.append(O);
        s.append("   ");
        s.append(O);
        s.append(O);
        s.append(O);
        return s;
    }
}

string UIpainter::paintUpperCardAscii(std::vector<card>& ref,short index)
{
    string upperAscii;

    upperAscii.append("|");
    upperAscii.append(ref.at(index).describeValue() );
    upperAscii.append(" ");
    upperAscii.append(ref.at(index).describeType() );
    upperAscii.append("|");
    upperAscii.append(" ");

    return upperAscii;
}

string UIpainter::paintUpperCardAscii(short slot)
{
    string upperAscii;

    if (Reps[slot].handVisible()) //Har data, ergo vis.
    {
        vector<card>& handref = Reps[slot].getHandRef();
        upperAscii.append("|");
        upperAscii.append(handref.at(0).describeValue());
        //upperAscii=PlayerHand.at(0).describeValue();
        upperAscii.append(" ");
        upperAscii.append(handref.at(0).describeType());
        upperAscii.append("|");

        upperAscii.append(" ");
        upperAscii.append("|");
        upperAscii.append(handref.at(1).describeValue());
        upperAscii.append(" ");
        upperAscii.append(handref.at(1).describeType());
        upperAscii.append("|");
    }
    else //hvis ikke tilhører hånden en annen spiller, og den skal ikke ennå vises.
    {
        upperAscii.append("|");
        upperAscii.append("?");
        upperAscii.append(" ");
        upperAscii.append("?");
        upperAscii.append("|");

        upperAscii.append(" ");
        upperAscii.append("|");
        upperAscii.append("?");
        upperAscii.append(" ");
        upperAscii.append("?");
        upperAscii.append("|");
    }
    return upperAscii;
}

string UIpainter::paintLowerCardAscii(std::vector<card>& ref,short index)
{
    string cc;

    cc.append("|");
    cc.append(ref.at(index).describeType() );
    cc.append(" ");
    cc.append(ref.at(index).describeValue() );
    cc.append("|");
    cc.append(" ");

    return cc;
}

string UIpainter::paintLowerCardAscii(short slot )
{
    string lowerAscii;

    if (Reps[slot].handVisible()) //Har data, ergo vis.
    {
        vector<card>& handref = Reps[slot].getHandRef();
        lowerAscii.append("|");
        lowerAscii.append(handref.at(0).describeType());
        lowerAscii.append(" ");
        lowerAscii.append(handref.at(0).describeValue());
        lowerAscii.append("|");

        lowerAscii.append(" ");
        lowerAscii.append("|");
        lowerAscii.append(handref.at(1).describeType());
        lowerAscii.append(" ");
        lowerAscii.append(handref.at(1).describeValue());
        lowerAscii.append("|");
    }
    else //hvis ikke tilhører hånden en annen spiller, og den skal ikke ennå vises.
    {
        lowerAscii.append("|");
        lowerAscii.append("?");
        lowerAscii.append(" ");
        lowerAscii.append("?");
        lowerAscii.append("|");

        lowerAscii.append(" ");
        lowerAscii.append("|");
        lowerAscii.append("?");
        lowerAscii.append(" ");
        lowerAscii.append("?");
        lowerAscii.append("|");
    }
    return lowerAscii;
}
