#ifndef HOLDEMGUI_H
#define HOLDEMGUI_H

#include "protocolenums.h"
#include "handwidget.h"

#include <QWidget>
#include <QMenuBar>
#include <QTextEdit>
#include <QListView>
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QGraphicsEffect>
#include <QKeyEvent>

typedef std::vector<uint> vecUint;

class worker;

class HoldemGUI : public QWidget
{
    Q_OBJECT

private:

    worker* conn;

    //meny relaterte variabler
    QMenuBar* bar;
    QMenu* game;
    QMenu* name;
    QAction* startgameAction;
    QAction* refillAction;
    QAction* transferAction;
    QAction* changeNameAction;
    QAction* leaveRoomAction;
    QAction* logoutAction;

    //Vi trenger et sted å legge kortene
    QWidget* tableView;

    //objekter inne i viewet
    handWidget* players[MAX_PLAYERS];
    QLabel* communitySlot[5];//max 5 kort i communityHand
    QLabel* potField;

    //objects just south of the tableview.

    QPushButton* callButton;
    QPushButton* allinButton;
    QPushButton* raiseButton;
    QPushButton* foldButton;

    //Bruker et layout for å ordne knappene horisontalt
    QBoxLayout* buttonLayout;
    QTextEdit* chatArea;
    QTextEdit* consoleArea;

    QLineEdit* inputField;

public:
    HoldemGUI(QWidget *parent = 0);

    //Brukes for å gjøre holdem objektet klar over worker, slik at man kan kommunisere med det, og indirekte med server.
    void hereIam(worker* connection);

protected:
    void keyPressEvent(QKeyEvent* event);

private:
    void setupIcons();//hjelpefunksjon, holder konstruktøren ren for kode.

    QString createCardUrl(ushort type,ushort value);//tilsvarende som handwidgets har, lager ikke en helt ny widget klasse bare for communitycards
    void setPot(uint value);//setter potten

private slots:
    //samtlige "onXX" funksjoner representerer implementasjonen av ett knapptrykk eller annen GUI input.
    void onCall();
    void onAllin();
    void onRaise();
    void onFold();

    void onStartGame();
    void onRefill();
    void onTransfer();

    void onInput();
    void onNameChange();
    void onLeaveRoom();

public slots:
    //velger å lage et begrenset antall slots of signals heller enn å ha en slot/signal per little handling.
    //I tillegg gjenbrukes protocolenums, noe som reduserer unødvendige terminologi.
    //overloading gjør det enkelt å invokere riktig funksjon.
    void genericReceiver(vecUint info, QString statement, uint opcode, QString extra);
    void genericReceiver(uint opcode,uint value,uint value2,uint value3);
    void genericReceiver(uint opcode,uint value,uint value2);
    void genericReceiver(uint opcode,uint value);
    void genericReceiver(uint opcode);
    void chatReceiver(vecUint message, QString name);               //spesielt tilpasset chat.

    //brukes i worker for å rense objektet for gammel GUI data fra forrige tilkobling.
    void clearGUI();

signals:
    void ChatMessageToSocket(QString text);
    void DisplayNameChange(QString name);

    void GenericCommand(uint opcode);
    void GenericCommand(uint opcode,uint value);
    void GenericCommand(uint opcode,uint value,uint value2);

};

#endif // HOLDEMGUI_H
