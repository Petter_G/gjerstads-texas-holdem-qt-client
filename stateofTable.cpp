#include "stateofTable.h"

stateofTable::stateofTable(std::vector<card>& communityHandref,int potref,short currentPlayerTableNrref,short currentButtonTableNrref,short ClientTableNrref,
                std::vector<playerRepresentationClient>& Repsref,std::vector<WinnerData>& WinnerInforef)

                : communityHand(communityHandref),
                pot(potref),
                currentPlayerTableNr(currentPlayerTableNrref),
                currentButtonTableNr(currentButtonTableNrref),
                ClientTableNr(ClientTableNrref),
                Reps(Repsref),
                WinnerInfo(WinnerInforef)

                 {}

std::vector<card>& stateofTable::getCommunityHand()
{
    return communityHand;
}

int stateofTable::getPot()
{
    return pot;
}

short stateofTable::getCurrentPlayer()
{
    return currentPlayerTableNr;
}

short stateofTable::getCurrentButton()
{
    return currentButtonTableNr;
}

short stateofTable::getClientTableNr()
{
    return ClientTableNr;
}

std::vector<playerRepresentationClient>& stateofTable::getReps()
{
    return Reps;
}

std::vector<WinnerData>& stateofTable::getWinnerData()
{
    return WinnerInfo;
}
