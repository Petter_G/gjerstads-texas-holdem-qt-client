TEMPLATE = app
TARGET = G_Holdem

QT += core
QT += gui
QT += widgets
QT += network widgets
QT += network

CONFIG += console
CONFIG -= app_bundle

LIBS += -pthread
CONFIG += c++14
CONFIG += c++14 -pthread

SOURCES += \
    client.cpp \
    card.cpp \
    playerrepresentationclient.cpp \
    uipainter.cpp \
    winnerdata.cpp \
    holdemgui.cpp \
    logingui.cpp \
    threadedworker.cpp \
    handwidget.cpp \
    loungegui.cpp

#include(deployment.pri)
#qtcAddDeployment()

HEADERS += \
    client.h \
    card.h \
    playerrepresentationclient.h \
    uipainter.h \
    protocolenums.h \
    winnerdata.h \
    holdemgui.h \
    logingui.h \
    threadedworker.h \
    handwidget.h \
    loungegui.h

FORMS +=

RESOURCES += \
    cards.qrc

