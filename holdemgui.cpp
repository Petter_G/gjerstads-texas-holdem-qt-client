#include "holdemgui.h"
#include "protocolenums.h"
#include "threadedworker.h"

#include <iostream>
#include <QDebug>
#include <thread>


HoldemGUI::HoldemGUI(QWidget* parent) : QWidget(parent)
{
    //meny relaterte data
    bar = new QMenuBar();
    game = new QMenu("&Game");
    name= new QMenu("&User");
    bar->addMenu(game);
    bar->addMenu(name);
    refillAction = new QAction(tr("refill cheat"),this);
    transferAction = new QAction(tr("escrow transfer"),this);
    changeNameAction = new QAction(tr("change name"), this);
    leaveRoomAction = new QAction(tr("Leave room"),this);
    logoutAction = new QAction(tr("logout"),this);
    game->addAction(refillAction);
    game->addAction(transferAction);
    name->addAction(leaveRoomAction);
    name->addAction(logoutAction);
    name->addAction(changeNameAction);

    //Rimelig selvforklarende hva jeg gjør her.
    callButton = new QPushButton(tr("&Call"));
    allinButton = new QPushButton(tr("&Allin"));
    raiseButton = new QPushButton(tr("&Raise"));
    foldButton = new QPushButton(tr("&Fold"));

    //Bruker et layout for å ordne knappene horisontalt
    buttonLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    buttonLayout->addStretch();
    buttonLayout->addWidget(callButton);
    buttonLayout->addWidget(raiseButton);
    buttonLayout->addWidget(foldButton);
    buttonLayout->addWidget(allinButton);
    buttonLayout->addStretch();

    //Vi trenger et sted å legge kort, pot, bank balanser osv i et view, et enkelt QWidget gjør jobben. en egen privat funksjon
    //blir brukt til grovarbeidet, slik at vi ikke fyller opp konstruktøren med masse logikk.
    tableView = new QWidget;
    tableView->setObjectName("tableView");

    //chat
    chatArea = new QTextEdit;
    chatArea->setReadOnly(true);

    //Forresten, la oss ha ett konsol vindu i tilegg.
    consoleArea = new QTextEdit;
    consoleArea->setReadOnly(true);

    //inputfield, brukes for å ta chatinput.
    inputField = new QLineEdit;
    inputField->setPlaceholderText(tr("Write your message here."));

    //Vi bruker en rekke layouts til å posisjonere widgetene slik vi ønsker.
    QHBoxLayout* textChannelLayout = new QHBoxLayout;
    QHBoxLayout* inputFieldLayout = new QHBoxLayout;

    textChannelLayout->addWidget(chatArea);
    textChannelLayout->addWidget(consoleArea);

    inputFieldLayout->addWidget(inputField);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->setMenuBar(bar);
    mainLayout->addWidget(tableView);
    mainLayout->addLayout(buttonLayout);
    mainLayout->addLayout(textChannelLayout);
    mainLayout->addLayout(inputFieldLayout);

    //Her er den private funksjonen jeg snakket om, den oppretter og flytter forskjellige widgets til faste posisjoner i viewet.
    setupIcons();

    tableView->setStyleSheet("QWidget#tableView { border-image: url(:/images/welcome.png) 0 0 0 0 stretch stretch;"
                             "background-repeat: no-repeat;"
                             //"border-image: url; "
                             "background-color: green}");//PS, dette er en hack, da Qt ikke støtter background-image.

    //this->showMaximized();
    setLayout(mainLayout);

    //knappene må kobles til lokale funksjoner, slik at de faktisk gjør noe.
    connect(callButton,SIGNAL(clicked()),this,SLOT(onCall()) );
    connect(allinButton,SIGNAL(clicked()),this,SLOT(onAllin()) );
    connect(raiseButton,SIGNAL(clicked()),this,SLOT(onRaise()) );
    connect(foldButton,SIGNAL(clicked()),this,SLOT(onFold()) );

    connect(inputField,SIGNAL(returnPressed()),this,SLOT(onInput()));
    connect(refillAction,SIGNAL(triggered()),this,SLOT(onRefill()) );
    connect(transferAction,SIGNAL(triggered()),this,SLOT(onTransfer()) );

    connect(changeNameAction,SIGNAL(triggered()),this,SLOT(onNameChange()) );
    connect(leaveRoomAction,SIGNAL(triggered()),this,SLOT(onLeaveRoom()));
}

void HoldemGUI::clearGUI()
{
    inputField->clear();

    for(auto* player : players)
    {
        player->hide();
        player->toggleYOULabel(false);
    }

    for(auto* slot : communitySlot)
    {
        slot->clear();
    }

    chatArea->clear();
    consoleArea->clear();
}

void HoldemGUI::hereIam(worker* connection)
{
    conn = connection;

    //Har nå kobling, kan nå koble opp mot worker trådens slots.
    connect(this,SIGNAL(ChatMessageToSocket(QString) ),conn,SLOT(writeMessageToSocket(QString)) );
    connect(this,SIGNAL(DisplayNameChange(QString)),conn,SLOT(writeDisplayNameChangedToSocket(QString)) );

    connect(this,SIGNAL(GenericCommand(uint)),conn,SLOT(genericCommandReceiver(uint)) );
    connect(this,SIGNAL(GenericCommand(uint,uint)),conn,SLOT(genericCommandReceiver(uint,uint)) );
    connect(this,SIGNAL(GenericCommand(uint,uint,uint)),conn,SLOT(genericCommandReceiver(uint,uint,uint)) );

    connect(logoutAction,SIGNAL(triggered()),conn,SLOT(disconnectFromServerHost()) );
}

//protected functions
void HoldemGUI::keyPressEvent(QKeyEvent* event)
{
    switch(event->key() )
    {
        case Qt::Key_C://call/check
            this->onCall();
            break;
        case Qt::Key_R:
            this->onRaise();
            break;
        case Qt::Key_A:
            this->onAllin();
            break;
        case Qt::Key_F:
            this->onFold();
            break;
        case Qt::Key_Return://hvis man trykket enter et eller annet sted betyr det at man vil skrive inn noe i chatten.
            {
                if ( !inputField->hasFocus() )//hvis inputfield ikke alt har fokus, skal ett return bety at denne får fokus
                {
                    inputField->setFocus();
                }
                else//hvis inputField alt har fokus, skal et nytt return bety at den skal miste fokus
                {
                    inputField->clearFocus();
                }
                break;
            }
    }
}

//private functions

//hjelpefunksjon, holder konstruktøren ren.
void HoldemGUI::setupIcons()
{
    ushort horizontalOffset = 0;

    //Vi trenger å pakke alt dette inni et layout.
    QHBoxLayout* viewLayout = new QHBoxLayout; //dette er layouten tableView vil settes til å bruke.
    QVBoxLayout* visibleLayout = new QVBoxLayout;//her legges alt synelig.

    QHBoxLayout* UVL = new QHBoxLayout; //UVL = UpperVisibleLayout
    QHBoxLayout* MVL = new QHBoxLayout; //ditto hvor M betyr middle
    QHBoxLayout* LVL = new QHBoxLayout; //ditto hvor L betyr lower.

    //legg disse til visibleLayout
    visibleLayout->addLayout(UVL);
    visibleLayout->addStretch();
    visibleLayout->addLayout(MVL);
    visibleLayout->addStretch();
    visibleLayout->addLayout(LVL);

    viewLayout->addLayout(visibleLayout);

    //nå kan vi gå videre med å fylle opp de forskjellige layoutene inne i visibleLayout med ting.

    ushort upperSlotLimit= MAX_PLAYERS/2;

    //players, setter opp slotene riktig. Dette blir de øvre slotter
    for (int i = 0;i<upperSlotLimit;i++)
    {
        players[i] = new handWidget(this,UPPER_PLAYER_ANIMATION);
        UVL->addWidget(players[i]);
        players[i]->toggleOpaque(true);

        //løsning på hide() problemet
        QSizePolicy pol = players[i]->sizePolicy();
        pol.setRetainSizeWhenHidden(true);
        players[i]->setSizePolicy(pol);
    }

    //nedre
    for(int i = upperSlotLimit;i<MAX_PLAYERS;i++)
    {
        players[i] = new handWidget(this,LOWER_PLAYER_ANIMATION);
        LVL->insertWidget(0,players[i]);
        horizontalOffset++;
        //players[i]->hide();
        players[i]->toggleOpaque(true);

        //løsning på hide() problemet
        QSizePolicy pol = players[i]->sizePolicy();
        pol.setRetainSizeWhenHidden(true);
        players[i]->setSizePolicy(pol);
    }

    //community

    for(int i = 0; i< 5;i++)
    {
        communitySlot[i] = new QLabel;
        communitySlot[i]->setPixmap(QPixmap(tr(":/images/blank.png")));
        communitySlot[i]->setFixedSize(71,96);

        QSizePolicy pol = players[i]->sizePolicy();
        pol.setRetainSizeWhenHidden(true);
        communitySlot[i]->setSizePolicy(pol);

        MVL->addWidget(communitySlot[i]);

        communitySlot[i]->clear();
    }

    potField = new QLabel;
    potField->setFixedWidth(200);
    potField->setFixedHeight(40);

    potField->setStyleSheet(
        "font-family: Times New Roman;"
        "background: darkblue;"
        "border-style: inset;"
        "color: ghostwhite;"
        "border-color: darkblue;"
        "border-width: 3px;"
        "font: bold 26px;");

    this->setPot(0);
    MVL->addWidget(potField);

    //pakk alt dette inn i viewlayout.
    tableView->setLayout(viewLayout);
}

QString HoldemGUI::createCardUrl(ushort type,ushort value)
{
    QString imageURL=resourceBase;
    imageURL.append(QString::number(type) );

    if (value < 10)
    {
        imageURL.append("0");
        imageURL.append(QString::number(value) );
    }
    else
        imageURL.append(QString::number(value) );

    imageURL.append(".gif" );
    return imageURL;
}

void HoldemGUI::setPot(uint value)
{
    const QString prefix = "In pot: $";
    QString appendage;
    appendage = prefix;

    appendage.append(QString::number(value));
    potField->setText(appendage);
}

//private slots

void HoldemGUI::onCall()
{
    emit GenericCommand(CALL);
}

void HoldemGUI::onAllin()
{
    emit GenericCommand(ALLIN);
}

void HoldemGUI::onRaise()
{
    bool response;
    uint raisevalue = QInputDialog::getInt(this,tr("Raise"),tr("By how much would you like to raise?"),
                    20,0,2147483647,10,&response);

    if (response)
        emit GenericCommand(RAISE,raisevalue );
}

void HoldemGUI::onFold()
{
    emit GenericCommand(FOLD);
}

void HoldemGUI::onInput()
{
    QString input = inputField->displayText();
    inputField->clear();
    if (input.size() == 0 )
        return;

    emit ChatMessageToSocket(input);
}

void HoldemGUI::onStartGame()
{
    //emit GenericCommand(STARTGAME);
}

void HoldemGUI::onRefill()
{
    bool response;
    int value = QInputDialog::getInt(this,tr("Escrow refill"),tr("please specify by how much you want to increase your escrow account:"),
                                        10,0,2147483647,10,&response);

    emit GenericCommand(REFILL,value);
}

void HoldemGUI::onTransfer()
{
    bool response;
    int value = QInputDialog::getInt(this,tr("Balance transfer:"),tr("please specify how much money you want to transfer from your escrow account."),
                                        10,0,2147483647,10,&response);
    if ( response )
        emit GenericCommand(TRANSFER,value);
}

void HoldemGUI::onNameChange()
{
    bool response;
    QString newname = QInputDialog::getText(this,tr("Change your nick?"),tr("Please enter your new username:"),
                        QLineEdit::Normal,QDir::home().dirName(),&response);

    if (response && !newname.isEmpty() )
    {
        emit DisplayNameChange(newname);
    }
}

void HoldemGUI::onLeaveRoom()
{
    QMessageBox box;
    box.setText("Are you sure?");
    box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);

    int ret = box.exec();

    if ( ret == QMessageBox::Ok )
    {
        emit GenericCommand(LEAVEROOM);
    }
}

using namespace std;
//public slots
void HoldemGUI::genericReceiver(vecUint info, QString statement, uint opcode,QString extra)
{
    switch (opcode)
    {
        case TABLESLOTCHANGEDNAMENOTIFY:
        {//opcode-tableslot-silent-namelength-character(1-20)
            QString& oldName = statement;
            QString& newName = extra;

            //hvis silent er false, skal det gis en liten bemerkning i konsolvinduet.
            if ( !(bool)info[2] )
            {
                QString appendage;
                appendage.append(oldName);
                appendage.append(" is now known as '");
                appendage.append(newName);
                appendage.append("'");
                consoleArea->append(appendage);
            }

            ushort index = info[0]-1;//tableslot = indeks +1, så kompenserer.
            players[index]->setPlayername(extra);

            break;
        }
        case HANDUPDATE:
        {//tableslot-card1type-card1value-card2type-card2value
            short index = info[0]-1;
            players[index]->setGFXleftCard(info[1],info[2]);
            players[index]->setGFXrightCard(info[3],info[4]);
            break;
        }
    }
}

void HoldemGUI::genericReceiver(uint opcode, uint value, uint value2, uint value3)
{
    switch(opcode)
    {
        case BETUPDATE:
        {
            short index = value-1;
            short bet = value2;
            short totalbet=value3;
            players[index]->setBet(bet,totalbet);
            break;
        }
        case TABLESLOTNOTIFY:
        {
            ushort index = value-1;
            bool local = (bool) value2;
            bool trulyNew = (bool)value3;
            QString appendage;

            //if local, then the data is about this player.
            if (local)
            {
                //if first, ie the data is about this particular player and not someone else, display an appropriate message
                appendage.append(tr("Welcome, you have been assigned tableslot "));
                appendage.append( QString::number(value) );
                consoleArea->append(appendage);

                //notify the corresponding handwidget and tell it to show the YOU label.
                players[index]->toggleYOULabel(true);
            }
            else//someone elses slot
            {
                //display a note about the new player's arrival. If trulyNew, show notice. If not, say nothing.
                if (trulyNew)
                {
                    appendage.append(tr("New player arrived, tableslot assigned is "));
                    appendage.append( QString::number(value) );
                    consoleArea->append(appendage);
                }

                //by default other player's hands are to be be hidden, Ie backside up.
                players[index]->showBackside();
            }

            players[index]->show();
            players[index]->showCards();

            break;
        }
    }
}

void HoldemGUI::genericReceiver(uint opcode,uint value,uint value2)
{
    switch(opcode)
    {
        case CURRENTPLAYERNOTIFY:
        {
            //reaktiver opaque på currentPlayer
            ushort oldCurrentPlayer = value2;
            ushort newCurrentPlayer = value;

            ushort oldCurrentPlayerIndex = oldCurrentPlayer-1;
            ushort newCurrentPlayerIndex = newCurrentPlayer-1;

            //hack for situasjonen når spillet er helt nytt, hvis oldCurrentPlayerIndex er over 7(aka MAX_PLAYERS-1), ignorer old.
            if ( oldCurrentPlayerIndex < MAX_PLAYERS )
                players[oldCurrentPlayerIndex]->toggleOpaque(true);

            players[newCurrentPlayerIndex]->toggleOpaque(false);
            break;
        }
        case BALANCEUPDATE:
        {
            short index = value-1;
            short balance = value2;
            players[index]->setBalance(balance);
            break;
        }
        case ACTIVEUPDATE://MISSTENKSOMT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            short index = value-1;
            if ( !( (bool)value2) )//aka inactive
            {
                players[index]->markBetFolded();
                players[index]->hideCards();
            }
            break;
        }
        case COMMUNITYCARDADDED:
        {
            for(auto* slot : communitySlot)
            {
                //må sjekke om slotten alt er i bruk
                if (slot->pixmap() == 0 )
                {
                    QString url = createCardUrl(value,value2);
                    std::cout<<"value of createCardUrl: "<<url.toStdString()<<std::endl;

                    slot->setPixmap( QPixmap(createCardUrl(value,value2)) );
                    slot->show();
                    break;
                }
            }
            break;
        }
    }
}

void HoldemGUI::genericReceiver(uint opcode, uint value)
{
    switch(opcode)
    {
        case PLAYERLEFTLOCALROOMNOTIFY:
        {
            short tableindex = value-1;
            players[tableindex]->markNameLeaving();
            players[tableindex]->hideCards();
            break;
        }
        case MINIMUMRAISEFAILURENOTIFY:
        {
            QString appendage;
            appendage.append(tr("Minimum raise is equal to the bigblind, which is "));
            appendage.append(QString::number(value) );
            appendage.append(". Please try another action.");
            consoleArea->append(appendage);
            break;
        }
        case POTNOTIFY:
        {
            setPot(value);
            break;
        }
        case CURRENTBUTTONNOTIFY:
        {
            ushort index = value-1;
            players[index]->toggleDealerButton(true);
            break;
        }
        case ALLINUPDATE:
        {
            ushort index = value-1;
            players[index]->markBetAllin();
            break;
        }
        case DELETEREP:
        {
            ushort index = value-1;
            players[index]->hide();
            break;
        }
    }
}

void HoldemGUI::genericReceiver(uint opcode)
{
    switch (opcode)
    {
        case REPAINT:
        {
            UIpainter* painter = conn->getpainter();
            string data;

            if (painter->getWinnerInfoSize() != 0)//ergo vi har winnerData
            {
                data = painter->returnAndWipeWinnerData();
                QString appendage;
                appendage.append(QString::fromStdString(data));
                consoleArea->append(appendage);
            }
            break;
        }
        //nullstiller alt av verdier, senere pakker modifiserer da denne "cleanslate" tilstanden hele bordet har.
        case TABLERESET:
        {

            for(int i = 0; i < MAX_PLAYERS;i++)
            {
                if (players[i] == nullptr)
                    continue;

                players[i]->setBet(0,0);
                players[i]->showBackside();
                players[i]->toggleDealerButton(false);
                players[i]->showCards();
            }
            //resetter community.
            for (auto* card : communitySlot)
            {
                card->clear();
            }

            consoleArea->append(tr("New round started."));
            break;
        }
        case PLAYERSNOTREADY:
        {
            consoleArea->append(tr("Unable to start: Not all players are ready."));
            break;
        }
        case VALIDCOMMAND:
        {
            consoleArea->append(tr("Valid Command"));
            break;
        }
        case INVALIDCOMMAND:
        {
            consoleArea->append(tr("Invalid Command"));
            break;
        }
        case NAMETOLONGNOTIFY:
        {
            consoleArea->append(tr("Your chosen name was to long."));
            break;
        }
        case PAUPERFAILURENOTIFY:
        {
            consoleArea->append(tr("You cannot afford that action. Please try again."));
            break;
        }
        case CHECKINVALIDNOTIFY:
        {
            consoleArea->append(tr("Checking is not currently possible."));
            break;
        }
        case NOTYOURTURNNOTIFY:
        {
            consoleArea->append(tr("It is not your turn."));
            break;
        }
        /*case REJECTCONNECTIONNOTIFY:
        {
            //obsolete, tas hånd om av
            QMessageBox box;
            box.setText(tr("Player limit reached, connection refused."));
            box.exec();
            break;
        }*/
        case GAMEHALTEDNOTIFY:
        {
            consoleArea->append(tr("The game is currently halted. This is due to the insufficient number of active players. "
                               "A minimum of 2 players is necessitated for playing Texasholdem.") );
            break;
        }
        case GAMEPAUSEDNOTIFY:
        {
            consoleArea->append(tr("The game is paused, please wait."));
            break;
        }
        case SERVERSHUTDOWNNOTIFY:
        {
            consoleArea->append(tr("Attention: Server is about to shut down."));
            break;
        }
        case NAMECHANGEINVALIDNOTIFY:
        {
            consoleArea->append(tr("Your attempt to change your username failed."));
            break;
        }
    }
}

void HoldemGUI::chatReceiver(vecUint message, QString name)
{
    QString appendage;
    appendage.reserve(message.size() + name.size() + 2);
    appendage.append(name);
    appendage.append(tr(": "));
    for(auto& val : message)
    {
        appendage.append(val);
    }
    chatArea->append(appendage);
}
