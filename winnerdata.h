#ifndef WINNERDATA_H
#define WINNERDATA_H

#include <vector>

//Liten klasse som holder og manipulerer data knyttet til winnerInfo sendt over til server
class WinnerData
{

    const int winnerTableslot;
    const int payout;
    const int potNumber;        //0 for mainpot, 1 osv for sidepots

public:
    WinnerData(std::vector<int> &data);

    int getWinner();
    int getPayout();
    int getPotNumber();
};

#endif // WINNERDATA_H
