#ifndef PLAYERREPRESENTATIONCLIENT_H
#define PLAYERREPRESENTATIONCLIENT_H

#include "card.h"

#include <vector>

//Variant av playerRepresentation, store likheter, likevel forskjellig. F.eks har denne klassen
//kun medlemmer for å sette de forskjellige feltene direkte, f.eks istedet for decrementBalance/increment, har man bare setBalance.
//Dette er fordi objektene til denne klassen er ment å representere en lokalt refererbar kopi av motstykke på serveren.
//Siden det er serveren som styrer alt, kan og bør funksjonene her forenkles, slik at de gjenspeiler sin rolle.
class playerRepresentationClient
{

private:
    std::vector<card> playerhand;
    std::string playerName;
    short tablenr;//kunne brukt socketfd, men da hadde bord numrene blitt rare. Bedre om pokergame ikke bryr seg om underliggende protokoller overhodet.
    int bet;
    int totalbet;
    int playerBalance;
    bool active;//brukes ved folds
    bool ready;//brukes ved oppstart av spill.
    bool allin;

public:

    playerRepresentationClient(short tablenrtemp);//standard, setter ikke navn, eller balanse, brukes under opprettelsen i server.

    void setName(std::string name);
    std::string getName();
    void addCard(card x);
    void dumpCards();

    void setBet(int bet);
    int getBet();

    void setTotalBet(int bettemp);
    int getTotalBet();

    void setBalance(int balance);
    int getBalance();

    void setAllin(bool state);
    bool getAllin();

    void setActiveState(bool activestate);
    bool getActiveState();

    void setReadyState(bool readystate);
    bool getReadyState();

    short getTableNr();
    bool handVisible();
    std::vector<card> &getHandRef();

};

#endif // PLAYERREPRESENTATIONCLIENT_H
