#ifndef HANDGUI_H
#define HANDGUI_H

#include "protocolenums.h"

#include <QWidget>
#include <QGraphicsEffect>
#include <QPixmap>
#include <QLabel>
#include <QTextEdit>
#include <QString>

#include <string>

class handWidget : public QWidget
{
    Q_OBJECT

    QLabel* bet;
    QLabel* balance;
    QLabel* playername;
    QLabel* leftCard;
    QLabel* rightCard;

    QLabel* button;
    QLabel* YOULabel;

    //Effekter kan ikke deles, må ha en for hvert kort.
    QGraphicsOpacityEffect* opacityONleft;
    QGraphicsOpacityEffect* opacityONright;

public:
    explicit handWidget(QWidget *parent = 0,bool orientation=0);
    ~handWidget();

    void setPlayername(QString name);

    void setGFXleftCard(short type,short value);    //tar informasjonen og oppdaterer venstre kort med riktig bilde.
    void setGFXrightCard(short type,short value);
    void showBackside();                               //setter begge kort til baksiden.
    void hideCards();                                  //gjemmer begge kort. Merk at dette ikke er ett synonym for Qs hide(),
                                                       //denne gjør bare at handwidgeten ikke tegner de to kortene, widgeten selv er fortsatt der
    void showCards();                                  //motsatt av over.

    void setBet(uint betvalue, uint totalbet);
    void markBetFolded(); //merker bet som folded
    void markNameLeaving();
    void markBetAllin();    //take a wild guess

    void setBalance(uint value);
    void toggleDealerButton(bool state);                               //true = button synlig, false = usynlig
    void toggleYOULabel(bool state);

    void toggleOpaque(bool state);//true for on

signals:

public slots:
};

#endif // HANDGUI_H
