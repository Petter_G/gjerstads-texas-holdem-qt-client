#include "playerrepresentationclient.h"

#include <string>
#include <iostream>

using namespace std;

playerRepresentationClient::playerRepresentationClient(short tablenrtemp)
{
    tablenr=tablenrtemp;
    playerName="Unnamed player";
    bet = 0;
    totalbet = 0;
    ready=false;
    active= true;
    allin=false;
}

void playerRepresentationClient::setName(string name)
{
    playerName = name;

}

string playerRepresentationClient::getName()
{
    return playerName;
}

void playerRepresentationClient::addCard(card x)
{
    playerhand.push_back(x);
}
void playerRepresentationClient::dumpCards()
{
    playerhand.clear();
}

void playerRepresentationClient::setBet(int bettemp)
{
    bet = bettemp;
}

void playerRepresentationClient::setTotalBet(int bettemp)
{
    totalbet = bettemp;
}

int playerRepresentationClient::getTotalBet()
{
    return totalbet;
}

void playerRepresentationClient::setBalance(int balance)
{
    playerBalance = balance;
}

int playerRepresentationClient::getBet()
{
    return bet;
}

int playerRepresentationClient::getBalance()
{
    return playerBalance;
}

void playerRepresentationClient::setAllin(bool state)
{
    allin=state;
}

bool playerRepresentationClient::getAllin()
{
    return allin;
}

void playerRepresentationClient::setActiveState(bool activestate)
{
    active = activestate;
}

bool playerRepresentationClient::getActiveState()
{
    return active;
}

void playerRepresentationClient::setReadyState(bool readystate)
{
    ready= readystate;
}

bool playerRepresentationClient::getReadyState()
{
    return ready;
}

short playerRepresentationClient::getTableNr()
{
    return tablenr;
}

bool playerRepresentationClient::handVisible()
{
    if (playerhand.size() == 0)
        return false;
    else
        return true;
}

vector<card>& playerRepresentationClient::getHandRef()
{
    return playerhand;
}
