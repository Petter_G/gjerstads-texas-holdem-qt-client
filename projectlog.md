# Note!
    Like noted in the readme, the revision history is incomplete, changes thus stretch back to October, 2016 only.
## Changes:
- 08.10.2016 
    - Replaced the new profile registration mechanic, now has a dedicated modal dialog window. Also fixed a few bugs related to profile creation requests on both sides, most of this was purely cosmetical, except for the redesign of opcode REGISTER, and the processing of it on both sides. Register now consists of two segments, OPCODE and "exists", which details whether the request was rejected due to the username already existing or not.

- 12.12.2016 
    - Fixed a bug related to CWD and symlinks. If the executable was launched indirectly, IE via a symlink, then the game would fail loading existing profiles as it would look for profiles.sav in the symlink's directory, not the game's directory.

- 12.01.2017 
    - DBProfileManager has been implemented, preliminary testing complete. The server can now store and retrieve profile information from a DB rather than file. The The old profileManager is still available, now known as the FileProfileManager, instead the profileManager is now a purely abstract interface for both File- and DBProfileManager(s).
    - In addition there is now support(limited) for storing configuration data to disc, in the form of config.cfg. This file is automatically created at startup of Server if it doesn't already exist, and is automatically loaded if it does. If it has to be created, the program exits after prompting the user to manually altering the file in order to properly configure the program, as there are now more than one execution path for the program at startup.
    - lastly some preparatory work has been done in anticipation of the planned "Proper server-side event handling" plan detailed in the project plan(ProsjektPlan.txt), which mainly means that the socket initialization has been extracted from the mainprocessloop, enabling the possibility of starting the processloop without actually listening yet. The user can use the command "start" in order to listen for and receive connections.

- 18.01.2017 
    - Documentation has been diversified. The README.me file now consists of the overview alone, while the patch history has been transferred to this file. In addition the overview itself has been revised. Same thing has been done to elements of the patch history.

- 23.01.2017 
    - Only smaller achanges today: Minor change to README.md, improved upon the server's user interface somewhat. Storage loading is no longer automated as it's now located in storageInit() which is called right after socketInit().  Made some more preparatory work in relation to the planned event handling upgrades.

- 27.03.2017 
    - Made some updates the handling of config.cfg, fixed a bug relating to starting the server when it is already started and other minor fixes.

- 06.04.2017 
    - fixed the fix from the last update where the unnecessary onDisconnect was merged with disconnectFromServer(). I forgot to reconnect the disconnected signal, which lead to the cleanup signals not being fired. This has now been fixed.

- 16.04.2017 
    - Made some minor changes in the client based on realizations made while implementing the java version. The c++ client remains poorly designed in many areas however, some cleanup is necessary to bring the code up to spec.

- 26.04.2017 
    - Fixed a few typos, updated the readme moved a few enums around. Nothing substantial.

- 30.05.2017 
    - While testing my Java port, I came across a major bug in the server that needs to be addressed. In HoldemPokerGame there is a vector called localReps(previously just Reps). That vector holds pointers to playerRepresentation objects that are assigned to that particular game. That vector will resize itself whenever a player leaves the room, potentially invalidating any index variables stored in HoldemPokerGame instances if a new player joins later on. To solve this problem without major redesigns, that vector shouldn't even be a vector, but a Array instead(not to confused with the basic raw array, I am talking about the STL container here).

    - So the following must be done: 

        - All functionality surrounding tableslots must be redesigned to be zero based rather than one based, IE making tableslots true euphemisms for their parent playerRepresentation's index in the localReps container.

        - localReps must become a STD::Arrays, with a size of MAX_PLAYERS(IE 8). That way any player leaving won't have any impact on existing stored index's validity.It does mean, however, that adding and removing players becomes more complex, which is imperative we get right.

        - The clients currently rely on getting 1 indexed tableslots. They ought to be redesigned rely on transmitted tableslot arguments to be indexes instead.

        - In hindsight, it was a bad idea not fully syncronizing tableslot with indexes from the getgo. If tableslots were effective euphemisms for HoldemPokerGames many index variables, then things would've been much easier to maintain.

        - On a different note. I've decided to backport the project format used in the Java port, IE this file will become projectlog.txt, the repository issue tracker will now be used and a conventional versioning system will be used from here on. I've decided that version 1.0.0 of the server will be formally released once I've implemented these changes.

- 18.06.2017  
    - The changes from May have been implemented, but in a quite different manner. Instead of making localReps a std::Arrays, I found out that it was much easier to simply convert the localReps vector to a std::map, and redesign any and all functionality that relied on indexes. This meant no changes to the clients, which already relied on getting tableslots anyway.

    - Preliminary testing was successful.

    - I also moved the configLoading namespace out of the protocolEnums.cpp file and into a couple of brand new files. I may want to do the same with the exceptions.

    - I hereby consider the May plans implemented. This version will be the first one I'll give a version, so this'll be version 0.1.0.

- 02.10.2017  
    - Many changes have been made since last time. Perhaps the biggest of which is the support for windows! That's right, the server can now be run on windows. There are some downsides though. The DBProfileManager is not yet supported, and the server can't take any console input. While the winsock2 API does a good job of implementing the BSD socket API without to many deviations, the lack of true file descriptors, like we know them in the unix world,means I can't rely on the neat little select trick to get syncronized console input. So for the time being it's simply disabled.

    - To facilitate the support for both platforms, server.cpp has been replaced by WINserver.cpp and UNIXserver.cpp. The header remains mostly unaltered, save for a few preprosessor switches, and some microsoft specific code.

    - On a different note: The join and create opcodes have been altered as to be in line with the new design, users now name their rooms upon creation, and optionally password protect them. Similarily, the join opcode has been altered in a similar fashion.

    - Another thing I might mention: I learned while porting the code that MS C++ doesn't support the c99 inspired extensions added by GCC, this lead to the altering of any code which relied on arrays of variable length.

  > As an addendum, making the code conformant with the MSVC led to an incredibly annoying bug: I had forgotten to update a loop relying on the c99 extensions. Which was bad enough, far worse was the fact that the GDB debugger gave faulty error messages, leading me away from where the bug actually was. 
  >
  > I spent days debugging the problem until I hit upon the idea of using the MSVC debugger instead. So I loaded up VS and ran the debugger. And voila, the problem became apparant straight away. I've kept the bugged code in the form of a comment under the now updated code. The bugged code consisted of a single line!
  >
  > I am not qualified to determine if this is a true bug in the debugger or not, although it most certainly looks to be one. The debugger consistently marked a non-bugged line of code as the culprit of the segfault. It did not give me accurate information, infact it was the exact opposite! It would've been better if it hadn't pointed at that non-faulty line at all, just stated that there was something wrong with the vector in some way. 
  >
  > Since new functionality has been added, version is now 0.2.0. Due to encoding issues and git automatically formating text files, I've decided to store the log as a md file instead. Should look prettier at least.

- 03.10.2017
    - Added a working windows executable, forgot to do that last time. Made some minor changes to executable name on the Unix version, updated the readme.
    - Reimplemented support for syncronized console input in the windows server.
	
- 04.10.2017
    - Enabled database support in the windows server. However I've yet to test this functionality due to not having a working server. Testing will commence once I got a mysql server back up and running. 
For now though, it looks to be alright. I had to use dynamic linking this time around as I was unable to link statically as I intended.

	- In my exhausted state the other day, I didn't realice that the console input wasn't being routed through the tunnel at all, merely being proccessed by a different
thread. Problem has been solved now though, an effective and true solution for the select problem has now been fully implemented.

    - Since new functionality has been implemented, version is now 0.3.0.

- 09.10.2017
    - Moved the client(this project) out of the original repo. The server now has the repo all to itself.
