#include "loungegui.h"

#include <QString>
#include <QStyledItemDelegate>
#include <QWidget>
#include <QMenuBar>
#include <QTextEdit>
#include <QListView>
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QGraphicsEffect>
#include <QKeyEvent>

#include <iostream>

using namespace std;



loungeGUI::loungeGUI(QWidget *parent) : QWidget(parent)
{
    //menus
    QMenuBar* bar = new QMenuBar;
    QMenu* userMenu = new QMenu("&User");
    changeNameAction = new QAction(tr("change name"), this);
    disconnectAction = new QAction(tr("logout"),this);

    bar->addMenu(userMenu);
    userMenu->addAction(changeNameAction);
    userMenu->addAction(disconnectAction);

    //logo
    QPixmap logo(":/images/transparantLogo.png");
    QLabel* logolabel = new QLabel;

    logolabel->setPixmap(logo);
    logolabel->setObjectName("logo");
    logolabel->setAlignment(Qt::AlignCenter);
    logolabel->setStyleSheet("background-color: green;");
    logolabel->setMinimumHeight(logo.height() * 3);
    logolabel->setMinimumWidth(logo.width()*1.05);

    create = new QPushButton(tr("Create"));
    join = new QPushButton(tr("Join"));

    chatArea = new QTextEdit;
    chatArea->setReadOnly(true);
    chatAreaScroller = new QScrollArea;
    chatAreaScroller->setWidget(chatArea);
    chatAreaScroller->setWidgetResizable(true);

    inputField = new QLineEdit;
    inputField->setPlaceholderText(tr("Write your message here."));

    //så settes selve viewet opp.
    //appropos: Userrole+1 = serversidefd,userrole+2  = gameid,
    //hvis userrole + 3 = er gyldig, betyr det at noden er et spill, og ikke en klient.
    //for slike noder skal role 1 og 2 ikke være satt til noe.
    model = new QStandardItemModel;
    rootptr = model->invisibleRootItem();

    //header, standard ser ut til å vise et 1 tall, ikke akkurat pent.
    QIcon icon(":/images/buttonGreen.gif");
    QStandardItem* header = new QStandardItem(icon,"Lounge");
    model->setHorizontalHeaderItem(0,header);

    //oppretter selve treeviewt og assosierer det med modelen.
    view = new QTreeView(this);
    view->setAlternatingRowColors(true);
    view->setSortingEnabled(true);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setObjectName("view");
    view->setStyleSheet("QTreeView::branch:has-siblings:!adjoins-item {"
                                        "border-image: url(:/images/vline.png) 0;}"

                        "QTreeView::branch:has-siblings:adjoins-item {"
                                        "border-image: url(:/images/branch-more.png) 0;}"

                        "QTreeView::branch:!has-children:!has-siblings:adjoins-item {"
                        "               border-image: url(:/images/branch-end.png) 0;}"

                        "QTreeView::branch:has-children:!has-siblings:closed,"
                        "QTreeView::branch:closed:has-children:has-siblings {"
                        "               border-image: none;"
                        "               image: url(:/images/branch-closed.png);}"

                        "QTreeView::branch:open:has-children:!has-siblings,"
                        "QTreeView::branch:open:has-children:has-siblings {"
                        "               border-image: none;"
                        "               image: url(:/images/branch-open.png);}");
    view->setModel(model);
    view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //layouts
    QHBoxLayout* logoLayout = new QHBoxLayout;
    logoLayout->addWidget(logolabel);

    buttonLayout = new QHBoxLayout;
    buttonLayout->addStretch();
    buttonLayout->addWidget(join);
    buttonLayout->addWidget(create);
    buttonLayout->addStretch();

    viewLayout = new QHBoxLayout;
    viewLayout->addWidget(view);

    chatLayout = new QVBoxLayout;
    chatLayout->addWidget(chatAreaScroller);
    chatLayout->addWidget(inputField);

    mainLayout = new QVBoxLayout;
    mainLayout->setMenuBar(bar);
    mainLayout->addLayout(logoLayout);
    mainLayout->addLayout(buttonLayout);
    mainLayout->addLayout(viewLayout);
    mainLayout->addLayout(chatLayout);

    this->setLayout(mainLayout);

    connect(join,SIGNAL(clicked()),this,SLOT(onJoin()) ) ;
    connect(create,SIGNAL(clicked()),this,SLOT(onCreate()) );
    connect(view,SIGNAL(clicked(QModelIndex)),this,SLOT(onItemSelected(QModelIndex)) );
    connect(view,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(onitemDoubleClicked(QModelIndex)) );

    connect(inputField,SIGNAL(returnPressed()),this,SLOT(onInput()) );
    connect(changeNameAction,SIGNAL(triggered()),this,SLOT(onNameChange()) );
}

loungeGUI::~loungeGUI()
{

}

//public
void loungeGUI::show_with_cookie(uint cookie)
{
    this->cookie=cookie;
    this->show();
}

//public slots

void loungeGUI::hereIam(worker* connection)
{
    conn = connection;

    //Har nå kobling, kan nå koble opp mot worker trådens slots.
    //PS: Dette er chicken and the egg problemet i ett nøtteskal, Jeg kan ikke kjøre connect mot conn objecktet før det eksisterer.
    //løsningen ser rar ut, men hva ellers?
    connect(this,SIGNAL(ChatMessageToSocket(QString) ),conn,SLOT(writeMessageToSocket(QString)) );
    connect(this,SIGNAL(DisplayNameChange(QString)),conn,SLOT(writeDisplayNameChangedToSocket(QString)) );
    connect(this,SIGNAL(GenericCommand(uint)),conn,SLOT(genericCommandReceiver(uint)) );
    connect(this,SIGNAL(GenericCommand(uint,uint)),conn,SLOT(genericCommandReceiver(uint,uint)) );
    connect(this,SIGNAL(GenericCommand(uint,uint,uint)),conn,SLOT(genericCommandReceiver(uint,uint,uint)) );
    connect(disconnectAction,SIGNAL(triggered()),conn,SLOT(disconnectFromServerHost()) );
}

void loungeGUI::genericReceiver(vecUint info, QString statement, uint opcode, vecUint extra)
{
    switch(opcode)
    {
        //nye spillere legges automatisk til ved roten.
        case NEWPLAYERNOTIFY:
        {
            QString name;

            int gameid = extra[0];
            uint serversidefd = extra[1];

            //lage en QString først:
            for(auto& letter : info)
            {
                name.append(letter);
            }

            //må si, dette var en rar greie digia fant opp: "user roles" og data.
            QStandardItem* item = new QStandardItem(name);
            item->setData(serversidefd,Qt::UserRole + 1);
            item->setData(gameid,Qt::UserRole + 2);

            //hvis gameid ikke er -1, er spilleren alt i et rom, legg ham der
            if (gameid != -1)
            {
                //let fram til til riktig gameid, plasser så spiller der.
                QModelIndex index;
                const QModelIndex rootindex = rootptr->index();
                int rows = model->rowCount(rootptr->index() );

                for (int r = 0;r < rows;r++)
                {
                    index = model->index(r,0,rootindex);
                    QVariant id = index.data(Qt::UserRole + 3);
                    if (id.isValid() && id.toInt() == gameid)//match
                    {
                        model->item(r,0)->appendRow(item);
                        return;
                    }
                }
            }
            else //spiller er en lounger
                model->insertRow(0,item);
            break;
        }
        case NAMECHANGEDNOTIFY:
        {
            //løp igjennom model på jakt etter riktig klient node
            //opcode-serversidefd-silent-tableslot-messagelength
            int serversidefd = info[0];

            const QModelIndex rootindex = rootptr->index();
            QModelIndex index;
            int rows = model->rowCount(rootindex);

            for (int r = 0; r < rows; r++)
            {
                index = model->index(r,0,rootindex);
                QVariant typeId = index.data(Qt::UserRole + 3);

                if (typeId.isValid() )//spill node
                {
                    int subrows = model->rowCount(index);
                    for (int j = 0; j < subrows;j++)
                    {
                        QModelIndex subindex = model->index(j,0,index);
                        QVariant storedFd = subindex.data(Qt::UserRole+1);
                        if (storedFd.isValid() && storedFd.toInt() == serversidefd )//match
                        {
                            //
                            model->itemFromIndex(subindex)->setData(statement,Qt::DisplayRole);
                            return;
                        }
                    }
                }
                else//klient node
                {
                    QVariant storedFd = index.data(Qt::UserRole+1);
                    if (storedFd.isValid() && storedFd.toInt() == serversidefd )//match
                    {
                        //
                        model->itemFromIndex(index)->setData(statement,Qt::DisplayRole);
                        return;
                    }
                }
            }

            //kommer vi hit betyr det at noden ikke fantes
            cout<<"Error in case: NAMECHANGENOTIFY: serversidefd("<<serversidefd<<") is not represented in local model."<<endl;
            break;
        }
    }
}

void loungeGUI::genericReceiver(uint opcode,uint value,uint value2,uint value3)
{
    switch(opcode)
    {
        case PLAYERSTATECHANGEDNOTIFY:
        {
            int serversidefd = value;
            int gameid = value2;
            bool dcstate = (bool)value3;

            const QModelIndex rootindex = rootptr->index();
            QModelIndex index;
            QModelIndex subindex;

            int rows = model->rowCount(rootindex);

            //let igjennom treet på jakt etter riktig node.
            for (int r = 0; r<rows;r++)
            {
                index = model->index(r,0,rootindex);
                QVariant typeId = index.data(Qt::UserRole+3);

                if (typeId.isValid() )//spillnode
                {
                    int subrows = model->rowCount(index);

                    for (int j = 0; j<subrows;j++)
                    {
                        subindex = model->index(j,0,index);
                        QVariant storedFd = subindex.data(Qt::UserRole+1);

                        if (storedFd.toInt() == serversidefd )//match.
                        {
                            //fjern denne, siden spiller nå er en lounger. Legg så til spillers node på nytt i rota.
                            QVariant displayname = subindex.data(Qt::DisplayRole);
                            QStandardItem* item = new QStandardItem(displayname.toString());

                            bool result = model->removeRow(j,index);
                            if ( !result ) //skal i teorien aldri feile, legger inn enkel debug mekanisme
                            {
                                cout<<"error in removing client item from room("<<typeId.toInt()<<")"
                                    "with the j-index of "<<j<<"."<<endl;
                                return;
                            }
                            //her legger vi til en ny node i rota HVIS dcstate er false. True betyr disconnect.
                            if ( !dcstate )
                            {
                                item->setData(serversidefd,Qt::UserRole + 1);
                                item->setData(-1,Qt::UserRole + 2);

                                model->insertRow(0,item);
                            }
                            return;
                        }
                    }
                }
                else    //klientnode
                {
                    QVariant storedFd = index.data(Qt::UserRole+1);
                    if (storedFd.toInt() == serversidefd )//match, siden vi fant spiller her, vet vi at det er en lounger som koblet fra.
                    {
                        bool result = model->removeRow(r,rootindex);
                        if ( !result ) //skal i teorien aldri feile, legger inn enkel debug mekanisme
                        {
                            cout<<"error in removing client item from room("<<typeId.toInt()<<")"
                                   "with the r-index of "<<r<<"."<<endl;
                        }
                        return;
                    }
                }
            }
            //Kommer vi så langt, er noe galt. Usannsynlig at feilen vil forårsake kræsj, så vi lar den være.
            cout<<"Error in case: PLAYERSTATECHANGEDNOTIFY: serversidefd is not represented in local model."<<endl;
            break;
        }
    }
}

void loungeGUI::genericReceiver(uint opcode,uint value,uint value2)
{
    switch(opcode)
    {
        case PLAYERJOINEDROOMNOTIFY:
        {
            int serversidefd = value;
            int gameid = value2;

            QModelIndex index;
            const QModelIndex rootindex = rootptr->index();

            bool loungerFound=false;
            int loungeRowValue;
            QString name;
            int rows = model->rowCount(rootindex);

            //først må vi finne klientens node, som for tiden er å finne i lounge delen.
            for (int r = 0; r < rows;r++)
            {
                if (!loungerFound)//vi er interessert i userrole +1, aka serversidefd
                {
                    index = model->index(r,0,rootindex);
                    QVariant var = index.data(Qt::UserRole + 1);
                    if (var.isValid() && var.toInt() == serversidefd )
                    {
                        //kult, vi har riktig node. Denne skal etterhvert fjernes, men vi venter litt, lagrer r og navnet;
                        loungeRowValue = r;
                        loungerFound = true;

                        QVariant var = index.data(Qt::DisplayRole);
                        name = var.toString();
                        continue;
                    }
                }
                else //vi er nå interessert i userrole + 3, dvs er noden en game node
                {
                    index = model->index(r,0,rootindex);
                    QVariant var = index.data(Qt::UserRole + 3);
                    if (var.isValid() && var.toInt() == gameid)//match
                    {
                        //ok, legg til en ny item her
                        QStandardItem* item = new QStandardItem(name);
                        item->setData(serversidefd,Qt::UserRole + 1);
                        item->setData(gameid,Qt::UserRole + 2);

                        model->itemFromIndex(index)->appendRow(item);
                    }
                }
            }
            //Til slutt må vi fjerne den gamle lounger item
            model->removeRow(loungeRowValue,rootptr->index());
            break;
        }
    }
}

void loungeGUI::genericReceiver(uint opcode,uint value)
{
    switch(opcode)
    {
        case NEWGAMENOTIFY:
        {
            int gameid = value;
            QString tip = QString("game %1").arg(gameid);

            QStandardItem* item = new QStandardItem(tip);
            item->setData(gameid,Qt::UserRole + 3);
            model->appendRow(item);
            break;
        }
    }
}

void loungeGUI::genericReceiver(uint opcode)
{
    switch(opcode)
    {
        case NAMECHANGEINVALIDNOTIFY:
        {
            chatArea->append(tr("Error: name change invalid"));
            break;
        }
        case JOINFAILURENOTIFY:
        {
            chatArea->append(tr("Attention: You are still technically active in another room, you will not "
                                "be able to join a new room until that room has finished processing that particular play."));
            break;
        }
    }
}

void loungeGUI::chatReceiver(vecUint message, int fd)
{
    //Vi må lete igjennom første set med noder, finne riktig node, navnet, og så hente denne.
    const QModelIndex rootindex = rootptr->index();
    QModelIndex index;
    int rows = model->rowCount(rootindex);

    for (int r = 0; r < rows;r++ )
    {
        index = model->index(r,0,rootindex);
        QVariant serversidefd = index.data(Qt::UserRole+1);//husk +1 betyr serversidefd

        if ( serversidefd.isValid() )
        {
            if ( serversidefd.toInt() == fd )//match
            {
                QVariant data = index.data(Qt::DisplayRole);
                QString appendage = data.toString();
                appendage.append(tr(": "));

                for(auto& val : message)
                    appendage.append(val);
                chatArea->append(appendage);
                return;
            }
        }
    }
}

void loungeGUI::clearGUI()
{
    //Bruker et layout for å ordne knappene horisontalt
    inputField->clear();
    //QStandardItemModel sin clear fjerner absolutt alt, til og med headers. Trenger en mer skånsom løsning
    int rows = model->rowCount(rootptr->index() );

    model->removeRows(0,rows,rootptr->index());

    chatArea->clear();
    cout<<"lounge clear called"<<endl;
}

void loungeGUI::onItemSelected(QModelIndex index)
{

    selectedIndex = index;
}

void loungeGUI::onitemDoubleClicked(QModelIndex index)
{
    selectedIndex = index;
    QVariant var = selectedIndex.data(Qt::UserRole +3);
    if (!var.isValid())
    {
        chatArea->append(tr("Please make sure to select a game-node before joining."));
        return;
    }

    int gameid = var.toInt();
    emit GenericCommand(JOIN,gameid,this->cookie);
}

void loungeGUI::onInput()
{
    //Å skrive dette bedre hadde betydd å skrive om hele protokollen, og hele server. Det skjer bare ikke.
    QString input = inputField->displayText();
    inputField->clear();
    if (input.size() == 0 )
        return;

    emit ChatMessageToSocket(input);
}

void loungeGUI::onJoin()
{
    cout<<"IN JOIN"<<endl;
    //sjekk om selectedindex er gyldig, hvis bruker brøver å koble til via join knappen før noe er merket får vi segfault.
    if (!selectedIndex.isValid() )
    {
        chatArea->append(tr("Please select a game node."));
        return;
    }

    //så må vi sjekke om indeksen peker på en spillnode og ikke en spillernode.
    QVariant var = selectedIndex.data(Qt::UserRole +3);
    if (!var.isValid())
    {
        chatArea->append(tr("Please select a game node."));
        return;
    }

    int gameid = var.toInt();
    emit GenericCommand(JOIN,gameid,this->cookie);
}

void loungeGUI::onCreate()
{
    emit GenericCommand(CREATE);
}

void loungeGUI::onNameChange()
{
    bool response;
    QString newname = QInputDialog::getText(this,tr("Change your nick?"),tr("Please enter your new username:"),
                        QLineEdit::Normal,QDir::home().dirName(),&response);

    if (response && !newname.isEmpty() )
    {
        emit DisplayNameChange(newname);
    }
}
