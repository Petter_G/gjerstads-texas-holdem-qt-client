#ifndef SERVERCONNECTOR_H
#define SERVERCONNECTOR_H

#include "uipainter.h"
#include "logingui.h"
#include "loungegui.h"

#include <vector>

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QtWidgets>
#include <QtNetwork>

class loginGUI;

class worker : public QObject
{
    Q_OBJECT

    loginGUI* login;
    QString serverIP;
    quint16 port;
    HoldemGUI* holdem;
    loungeGUI* lounge;

    QTcpSocket* socket;
    UIpainter* painter;


public:
    explicit worker(loginGUI* log, QString serverIP, quint16 serverport, HoldemGUI* Holdem, loungeGUI* Lounge);
    ~worker();

    const QTcpSocket *getSocket();
    UIpainter* getpainter();

private:
    //leser x blokker(int32), og returnerer en vektor av disse.
    std::vector<uint> BlockReader(uint blockCount, QDataStream &reader);

private slots:

    void incomingData();

public slots:

    void connectToServer();//åpenbart nok, kobler til server. Brukes av login og registerprofile
    void disconnectFromServerHost();//attempts to disconnect from server, tells the GUIs to reset themselves to a virgin state.

    void writeMessageToSocket(QString text);
    void writeDisplayNameChangedToSocket(QString name);

    void genericCommandReceiver(uint opcode);//brukes av startgame,check,call,fold og allin
    void genericCommandReceiver(uint opcode, uint value);//brukes kun av raise
    void genericCommandReceiver(uint opcode, uint value,uint value2);

    void doProfile(QString username, QString password, bool login);//brukes ved login, login = true at bruker trykket login, false register

signals:

    void signalGUI_resetYourself();

    void switch_to_loungeGUI(uint cookie);
    void switch_to_loungeGUI();//i motsetning til den over, kaller bare vanlig show();
    void switch_to_loginGUI();
    void switch_to_holdemGUI();
    void loginSuccess();

    void authenticationFailure();
    void registerNewProfileFailure(bool exists);
    void registerNewProfileSuccess();

    void appendHoldemChatArea(vecUint message,QString playername);
    void appendLoungeChatArea(vecUint message,int sockfd);
    //Istedet for å ha en drøss signaler, lager vi et fåtall overladede funskjoner.
    //holdem
    void genericHoldemSignal(vecUint data,QString playername,uint opcode,QString extra);
    void genericHoldemSignal(uint opcode,uint value,uint value2,uint value3);
    void genericHoldemSignal(uint opcode,uint value,uint value2);
    void genericHoldemSignal(uint opcode,uint value);
    void genericHoldemSignal(uint opcode);

    //lounge
    void genericLoungeSignal(vecUint data,QString playername,uint opcode,vecUint extra);
    void genericLoungeSignal(uint opcode,uint value,uint value2,uint value3);
    void genericLoungeSignal(uint opcode,uint value,uint value2);
    void genericLoungeSignal(uint opcode,uint value);
    void genericLoungeSignal(uint opcode);
};
Q_DECLARE_METATYPE(QAbstractSocket::SocketError* )
Q_DECLARE_METATYPE(vecUint)

#endif // TCP_QT_CLIENT_H
