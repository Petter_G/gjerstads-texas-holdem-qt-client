# Welcome to Gjerstads Texas Holdem Qt Client

##What is this now?
This repo is for the now defunct(for now atleast) Gjerstads Texas Holdem C++ client. 

This client was originally embedded in the the original repo. I decided to move it as the old repo was getting a little to crowded for my liking.

##So if this is obsolete...
Yeah, use this one instead: https://bitbucket.org/Petter_G/gjerstads-texas-holdem-java-client

The server project is still very much alive, now having the original repo all to itself over at: https://bitbucket.org/Petter_G/gjerstads-texasholdem