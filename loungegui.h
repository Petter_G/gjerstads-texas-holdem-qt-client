#ifndef LOUNGEGUI_H
#define LOUNGEGUI_H

#include "protocolenums.h"
#include "threadedworker.h"

#include <vector>

#include <QTreeView>
#include <QWidget>
#include <QStandardItemModel>
#include <QTextEdit>
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QLineEdit>

class loungeGUI : public QWidget
{
    Q_OBJECT

    worker* conn;

    //actions
    QAction* changeNameAction;
    QAction* disconnectAction;

    //Buttons
    QPushButton* create;
    QPushButton* join;

    QTextEdit* chatArea;
    QScrollArea* chatAreaScroller;

    QLineEdit* inputField;

    uint cookie;

    QTreeView* view;
    QStandardItemModel* model;
    QStandardItem* rootptr;
    QModelIndex selectedIndex;

    //layouts
    QHBoxLayout* buttonLayout;
    QHBoxLayout* viewLayout;
    QVBoxLayout* chatLayout;

    QVBoxLayout* mainLayout;

    //enkel privat widget for bruk som bakgrun for logo, vi vil at

public:
    explicit loungeGUI(QWidget *parent = 0);
    ~loungeGUI();

    void show_with_cookie(uint cookie);//egen variant av Qts show, brukes til å overføre cookie samtidig som den vanlige show() blir kalt.

signals:
    void ChatMessageToSocket(QString text);
    void DisplayNameChange(QString name);
    void GenericCommand(uint opcode);
    void GenericCommand(uint opcode,uint value);
    void GenericCommand(uint opcode,uint value,uint value2);

public slots:
    void hereIam(worker* connection);

    void genericReceiver(vecUint info, QString statement, uint opcode, vecUint extra);
    void genericReceiver(uint opcode,uint value,uint value2,uint value3);
    void genericReceiver(uint opcode,uint value,uint value2);
    void genericReceiver(uint opcode,uint value);
    void genericReceiver(uint opcode);
    void chatReceiver(vecUint message, int fd);               //spesielt tilpasset chat.

    void clearGUI();

private slots:
    void onItemSelected(QModelIndex index);
    void onitemDoubleClicked(QModelIndex index);

    void onInput();

    void onJoin();
    void onCreate();
    void onNameChange();//gjør det samme som sin identiske motpart i holdemGUI
};

#endif // LOUNGEGUI_H
