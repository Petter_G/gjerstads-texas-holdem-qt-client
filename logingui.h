#ifndef LOGINGUI_H
#define LOGINGUI_H

#include "threadedworker.h"
#include "holdemgui.h"
#include "handwidget.h"
#include "loungegui.h"

#include <QWidget>
#include <QDialog>
#include <QDebug>
#include <QLabel>
#include <QTextEdit>
#include <QLineEdit>
#include <QGroupBox>
#include <QThread>
#include <QAbstractSocket>
#include <QKeyEvent>

#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string>
#include <arpa/inet.h>

#include <memory>

class HoldemGUI;
class loungeGUI;
class registerDialogGUI;

class loginGUI : public QWidget
{
    Q_OBJECT

    registerDialogGUI* regDialog;
    QThread* thread;
    worker* work;
    //HoldemGUI* holdemGUI;
    std::unique_ptr<HoldemGUI> holdemGUI;
    std::unique_ptr<loungeGUI> lounge;
    QString serverIP;
    quint16 serverPort;

    //GUI
    QLabel* usernameLabel;
    QLabel* passwordLabel;
    QLineEdit* username;
    QLineEdit* password;
    QPushButton* loginButton;
    QPushButton* registerNewProfileButton;
    QGroupBox* groupbox;

public:
    explicit loginGUI(QWidget *parent, QString serverIPtemp, quint16 serverporttemp);
    ~loginGUI();

private:
    //void loadConfig();//laster inn preset fra fil.

protected:
    //bruker skal ikke behøve å fysisk trykke på login knappen
    void keyPressEvent(QKeyEvent* event);

signals:
    void HoldemGUIActive(HoldemGUI* ptr); //Brukes i loginSuccess, sender en peker tilbake til connector,
                                            //slik at den snart opprettede UIpainter(rename pending) kan snakke med HoldemGUI.
    void profile(QString usernameText,QString passwordText,bool type);
    void RegisterCallback(); //used to get loginGUI attempt the connection

public slots:
    void loginSuccess(uint cookie);
    void joinSuccess();
    void backtoLogin();
    void backToLounge();
    
    void displayError(QAbstractSocket::SocketError socketError);
    void displayAuthenticationFailure();
    void displayRegisterNewProfileFailure(bool exists);
    void displayRegisterNewProfileSuccess();
    void onRegisterNewProfile();

private slots:
    void onLogin();
    void onNewProfileRequested(QString user, QString pass);

};

class registerDialogGUI : public QDialog
{
    Q_OBJECT

    //Fields
    QLabel* usernameLabel;
    QLabel* passwordLabel;
    QLabel* confirmPassLabel;

    QLineEdit* username;
    QLineEdit* password;
    QLineEdit* confirmpass;

    //Buttons
    QPushButton* registerButton;
    QPushButton* cancelButton;

    //layouts
    QVBoxLayout* masterLayout;
    QVBoxLayout* fieldLayout;
    QHBoxLayout* buttonLayout;

    QHBoxLayout* usernameLayout;
    QHBoxLayout* passLayout;
    QHBoxLayout* confirmpassLayout;

public:
    explicit registerDialogGUI(loginGUI *login);

private slots:
    void onRegister();
    void onCancel();

signals:
    void doProfileRegistration(QString user,QString pass);

};

#endif // LOGINGUI_H
