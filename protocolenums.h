#ifndef PROTOCOLENUMS
#define PROTOCOLENUMS

#include <string>
#include <vector>
#include <QString>

typedef unsigned int uint;
typedef unsigned int BLOCKSIZE;
typedef std::vector<uint> vecUint;

const QString resourceBase(":/images/");

//0-99, kommandoer innkommende fra klient
enum SERVERCOMMANDS
{
    SETREADY = 10,
    CLEARREADY = 2,
    CHECK = 3,
    CALL = 4,
    FOLD = 5,
    RAISE = 6,
    ALLIN = 8,
    SETNAME=7,
    RESET=9,
    REFILL=11,       //opcode-amount //Brukes for å fylle opp konto.
    TRANSFER=12,     //opcode-amount //Ber om en overføring av penger fra escrow til konto. I praksis blir ikke overføringen fullført før neste parti.
    LOGIN = 13,       //opcode-userlength-passlength-username-password
    REGISTER = 14,    //opcode-userlength-passlength-username-password
    JOIN = 15,        //opcode-gameid-cookieid
    CREATE = 16,         //opcode
    LEAVEROOM = 17      //opcode

};

//100-199, beskjeder fra server. Kommentarene viser hvordan pakkene ser ut(hver post er en int).
enum CLIENTENUMS
{
    INVALIDCOMMAND = 100, //opcode
    TABLESLOTNOTIFY = 101,//opcode-tableslot-trulyNew-local
                        //trulyNew is a bool that denotes if the player is new to the game, or just that particular client.
                        //Used exclusively to create the proper console area notes.
                        //Local denotes if this notification relates to the local player.
    PLAYERSNOTREADY = 102,//opcode
    VALIDCOMMAND = 103,//opcode
    NAMECHANGEDNOTIFY = 104,//opcode-serversidefd-silent-tableslot-messagelength-character(1-20) variabel lengde //messagelength inkluderer kun meldingen selv.
                            //silent brukes til å styre hvorvidt det skal gis en "is now known as" melding i konsol vinduer.
    TABLESLOTCHANGEDNAMENOTIFY =141,//opcode-tableslot-silent-namelength-character(1-20)//brukes for å fortelle et roms pillere at en av dem har skiftet navn.
    REPAINT = 105,//opcode

    HANDUPDATE = 106,//opcode-tableslot-card1type-card1value-card2type-card2value
    DELETEREP = 107,//opcode-tableslot

    DUMPHANDUPDATE = 108,//opcode-tableslot
    BETUPDATE = 109,//opcode-tableslot-bet-totalbet
    BALANCEUPDATE = 110,//opcode-tableslot-balance
    ACTIVEUPDATE = 111,//opcode-tableslot-activestate
    UPDATEREADY = 112,//opcode-tableslot-activestate
    ALLINUPDATE = 123,//opcode-tableslot    //kalles aldri for å gjøre en allin tilstand false, ved communityreset skjer dette automatisk.

    NAMETOLONGNOTIFY = 113,//opcode
    TABLERESET = 114,//opcode
    COMMUNITYCARDADDED = 115,//opcode-card1type-card1value

    POTNOTIFY = 116, //opcode-potvalue
    CURRENTPLAYERNOTIFY = 117, //opcode-currentplayer
    CURRENTBUTTONNOTIFY = 118, //opcode-currentButton

    MINIMUMRAISEFAILURENOTIFY = 119, //opcode-bigblind
    PAUPERFAILURENOTIFY = 120, //opcode
    NOTYOURTURNNOTIFY = 121, //opcode
    CHECKINVALIDNOTIFY = 122, //opcode
    REJECTCONNECTIONNOTIFY = 124,//opcode
    GAMEHALTEDNOTIFY = 125,  //opcode
    GAMEPAUSEDNOTIFY = 128, //opcode

    WINNERINFONOTIFY = 126,  //opcode-winnertableslot-payout-potIndex
    JOINSUCCESSNOTIFY = 127, //opcode
    JOINFAILURENOTIFY = 142, //opcode
    //AUTHENTICATEREQUIREDNOTIFY = 129, //opcode //obsolete
    NAMECHANGEINVALIDNOTIFY = 130, //opcode
    AUTHENTICATIONFAILURENOTIFY = 131, //opcode
    AUTHENTICATIONSUCCESSNOTIFY = 132, //opcode-cookieid

    INVALIDPROFILECREATION = 133,    //opcode
    VALIDPROFILECREATION =134,       //opcode
    SERVERSHUTDOWNNOTIFY = 135,         //opcode
    NEWGAMENOTIFY = 136,             //opcode-gameid
    NEWPLAYERNOTIFY = 137,           //opcode-gameid-serversidefd-messagelength-character(1-20)
                                    //serverside fd brukes som ekstra data, slik at det er lettere å finne objektet hos klientene
                                    //fd for hver enkelt klient endrer seg ikke, og den gjenbrukes ikke før klienten er helt utlogget uansett
    //CLIENTDISSCONNECTEDNOTIFY = 138,           //opcode-serversidefd-gameid
    PLAYERJOINEDROOMNOTIFY = 139,      //opcode-serversidefd-gameid
    PLAYERLEFTLOCALROOMNOTIFY = 140,      //opcode-tableslot//brukes når spiller trekker seg fra et lokalt rom.
    PLAYERSTATECHANGEDNOTIFY = 143,    //opcode-serversidefd-gameid-dcstate //Erstatter CLIENTDISCONNECTNOTIFY. Brukes mot samtlige klienter etter at
    //spiller har enten har koblet fra, eller forlatt et rom. DCstate = true betyr DC. gameid = -1 betyr at gameid ikke er relevant(lounger)
    MAXROOMSCREATEDNOTIFY = 144            //opcode

};

//200-255 unused.
enum message
{
    SHORTCHAT  = 201, //opcode-character(1-2000)//variant brukt ved kommunikasjon sendt fra klient til server. Implementert for letthetens skyld
    CHAT = 200 //opcode-tableslot-fd-bytecount-character(1-2000) variabel lengde //-1 betyr lounger
};

enum cardValuesEnums
{
    ACELOW = 1,
    TWO = 2,
    THREE=3,
    FOUR=4,
    FIVE=5,
    SIX=6,
    SEVEN=7,
    EIGHT=8,
    NINE=9,
    TEN=10,
    JACK=11,
    QUEEN=12,
    KING=13,
    ACEHIGH = 14
};

enum cardTypesEnums
{
    CLUBS=0,
    DIAMONDS=1,
    HEARTS=2,
    SPADES=3
};

enum scoreValuesEnums
{
    ROYALFLUSH=10,
    STRAIGHTFLUSH=9,
    FOUROFAKIND=8,
    HOUSE=7,
    FLUSH=6,
    STRAIGHT=5,
    THREEOFAKIND=4,
    TWOPAIR=3,
    PAIR=2,
    HIGHCARD=1
};

enum BlockEuphemisms
{
    ONE_BLOCK = 1,
    TWO_BLOCKS = 2,
    THREE_BLOCKS = 3,
    FOUR_BLOCKS = 4,
    FIVE_BLOCKS = 5,
    SIX_BLOCKS = 6
};

//Diverse andre eufemismer
enum DIV
{
    MAX_MESSAGESIZE=2000,
    MAX_NAMESIZE=20,
    MAX_PLAYERS = 8,
    OPCODE_SIZE = 4,

    UPPER_PLAYER_ANIMATION = 1,
    LOWER_PLAYER_ANIMATION = 0,
};



#endif // PROTOCOLENUMS
