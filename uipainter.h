#ifndef PLAYERUI_H
#define PLAYERUI_H

#include "client.h"
#include "card.h"
#include "playerrepresentationclient.h"
#include "winnerdata.h"
#include "holdemgui.h"
#include "protocolenums.h"

#include <vector>
#include <string>
#include <algorithm>
#include <QObject>

//class Client;
class HoldemGUI;

class UIpainter
{

private:

    std::vector<card> communityHand;
    int pot;

    short currentPlayerTableNr;
    short currentButtonTableNr;
    short ClientTableNr;                            //spillerens egen tablenr
    std::vector<playerRepresentationClient> Reps;   //lokal variant av Reps fra server.
    std::vector<WinnerData> WinnerInfo;

public:

    UIpainter();
    //offentlige funksjoner knyttet til oppretting og oppdatering av reps ect.

    void resetObject();//reseter objektet. Brukes av worker hvis det blir brudd på kobling til server.

    bool createRep(short tableslot);
    void setHand(std::vector<int> dataparcel);//dumper eventuelle eksisterende kort, og legger til to nye.
    void setHand(vecUint dataparcel);         //overload tilpasning til det nye Qt baserte GUiet. renere sånn
    void updateRepName(QString newname, short tableslot);
    void updateBet(short tableslot, int bet, int totalbet);
    void updateBalance(int balance, short tableslot);
    void updateActive(bool active,short tableslot);
    void updateReady(bool ready, short tableslot);
    void updateAllIn(short tableslot);
    void deleteRep(short tableslot);

    //offentlige funksjoner knyttet til communityHand
    void resetTable();//kaller communityHands clear() og resetter hver spillers totalBet, samt dumper samtlige spilleres hender(tidligere DUMPHANDUPDATE)
    void addCardToCommunity(short type,short value);//gir community et kort per kall

    //offentlige funksjoner knyttet til oppdatering av variabler som ikke har med Reps å gjøre.
    void updatePot(short potupdate);
    void updateCurrentPlayer(short currentPlayer_Tablenr);
    void updateCurrentButton(short currentButton_Tablenr);
    void addWinnerData(std::vector<int>& data);          //Legger winner data til WinnerData.

    //eneste offentlige funksjon nødvendig printe ut et UI.
    void drawTable(bool exp);

    //div
    std::string getPlayerName(short tableslot);
    void setLocalTableNr(short tablenr);
    short getLocalTableNr();
    int getWinnerInfoSize();
    short getCurrentPlayerTableNr();
    std::vector<playerRepresentationClient> &getReps();

    std::string returnAndWipeWinnerData();         //tilpasset et GUI, istedet for å skrive alt rett ut til cout, lages en string som returneres

private:

    void printAndWipeWinnerData();               //skriver ut innholdet i WinnerInfo på en passe måte, for så å tømme WinnerInfo.

    void drawCommunity(std::string expCommunity, bool exp);
    std::string SU();
    std::string EU(std::string var, bool explain);
    std::string paintButtonFText(int tablnr);
    std::string paintBalanceFText(int index);
    std::string paintBetFText(int index);
    std::string paintFText(std::string name, int tablenr);
    std::string paintCardSpace(bool empty);
    std::string paintCardSpace();
    std::string paintLineAscii(bool linetype, bool community);
    std::string paintLineAscii(bool linetype);
    std::string paintUpperCardAscii(short slot);
    std::string paintUpperCardAscii(std::vector<card>&, short index);
    std::string paintLowerCardAscii(short slot);
    std::string paintLowerCardAscii(std::vector<card>&, short index);

};

#endif // PLAYERUI_H
