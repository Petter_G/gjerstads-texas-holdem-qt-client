#include "logingui.h"

#include "protocolenums.h"

#include <iostream>
#include <fstream>

loginGUI::loginGUI(QWidget *parent,QString serverIPtemp, quint16 serverporttemp) : QWidget(parent)
{
    serverIP = serverIPtemp;
    serverPort = serverporttemp;

    usernameLabel = new QLabel(tr("Username: "));
    usernameLabel->setFixedWidth(100);
    passwordLabel = new QLabel(tr("Password: "));
    passwordLabel->setFixedWidth(100);
    username = new QLineEdit();
    password = new QLineEdit();
    password->setEchoMode(QLineEdit::Password);
    loginButton = new QPushButton(tr("Login"));
    registerNewProfileButton = new QPushButton(tr("Register new profile"));

    QHBoxLayout* usernameLayout = new QHBoxLayout();
    QHBoxLayout* passwordLayout = new QHBoxLayout();

    usernameLayout->addStretch();
    usernameLayout->addWidget(usernameLabel);
    usernameLayout->addWidget(username);
    usernameLayout->addStretch();

    passwordLayout->addStretch();
    passwordLayout->addWidget(passwordLabel);
    passwordLayout->addWidget(password);
    passwordLayout->addStretch();

    QVBoxLayout* innerButtonLayout = new QVBoxLayout();
    innerButtonLayout->addWidget(loginButton);
    innerButtonLayout->addWidget(registerNewProfileButton);
    innerButtonLayout->addStretch();

    QHBoxLayout* outerButtonLayout = new QHBoxLayout();
    outerButtonLayout->addStretch();
    outerButtonLayout->addLayout(innerButtonLayout);
    outerButtonLayout->addStretch();

    QVBoxLayout* innerlayout = new QVBoxLayout();
    innerlayout->addLayout(usernameLayout);
    innerlayout->addLayout(passwordLayout);
    innerlayout->addLayout(outerButtonLayout);

    groupbox = new QGroupBox(tr("Welcome to Gjerstads Texas Holdem Poker"));
    groupbox->setLayout(innerlayout);
    groupbox->setStyleSheet("QGroupBox "
                            "{"
                            "   border: 2px solid grey;"
                            "   border-radius: 25px;"
                            "   margin-top: 2em;"
                            "   font-style: italic;"
                            "   font-size: 16px;"
                            "   font-weight: bold;"
                            "}"
                            "QGroupBox::title"
                            "{"
                            "   subcontrol-origin: margin;"
                            "}");

    QHBoxLayout* outerLayout = new QHBoxLayout();
    outerLayout->addStretch();
    outerLayout->addWidget(groupbox);
    outerLayout->addStretch();

    setLayout(outerLayout);

    //nullptr fordi vi vil at HoldemGUIet skal være selvstendig, ett hide av loginGUI skal ikke påvirke HoldemGUI
    holdemGUI = std::unique_ptr<HoldemGUI>(new HoldemGUI(nullptr) );
    lounge = std::unique_ptr<loungeGUI>(new loungeGUI(nullptr) );

    //vi starter opp worker, som har ansvaret for nettverket. Den er og trådet.
    thread = new QThread;
    work = new worker(this,serverIP,serverPort,holdemGUI.get(),lounge.get());
    work->moveToThread(thread);
    thread->start();

    //koblinger gjøres her.
    connect(loginButton,SIGNAL(clicked()),this,SLOT(onLogin() ) );
    connect(registerNewProfileButton,SIGNAL(clicked()),this,SLOT(onRegisterNewProfile()));
    connect(this,SIGNAL(profile(QString,QString,bool)),work,SLOT(doProfile(QString,QString,bool)) );

    show();
}

loginGUI::~loginGUI()
{
    delete work;
}

/*void loginGUI::loadConfig()//laster inn default ip og port fra fil. Gjør at vi ikke behøver å sette ip/port hver bidige gang, men samtidig ha kraftig konfigurerbarhet
{
    ifstream input;

    input.open("config.cnf");

    if ( !input.is_open() )
    {
        //hvis ikke åpen, antas default, som er localhost. Samt at en config.cnf opprettes.
        cout<<"\"config.cnf\" not available, creating new file with localhost as default."<<endl;
        ofstream filecreator("config.cnf");

        //skriv så ip og port til den tomme fila
        filecreator << "127.0.0.1"<<"\n"<<"3500"<<endl;

        //sett serverIP og serverPort for senere bruk
        serverIP = tr("127.0.0.1");
        serverPort = 3500;
        filecreator.close();
    }
    else
    {
        //les dataene inn
        input >> serverIP;
        input >> serverPort;
    }
}*/

void loginGUI::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
    {
        onLogin();
    }
}

void loginGUI::loginSuccess(uint cookie)
{
    //skjul loginGUI
    this->hide();

    //Vis loungeGUI,egen funksjon der
    lounge->show_with_cookie(cookie);
}

void loginGUI::joinSuccess()
{
    //skjul lounge
    lounge->hide();

    //show holdem
    holdemGUI->show();
}

void loginGUI::backtoLogin()
{
    //skjul de to andre, uavhengig av hvilke som er synlige.
    holdemGUI->hide();
    lounge->hide();
    this->show();
}

void loginGUI::backToLounge()
{
    holdemGUI->hide();
    this->hide();

    lounge->show();
}

void loginGUI::onLogin()
{
    emit profile(username->text(),password->text(),true );
    username->clear();
    password->clear();
}

void loginGUI::onNewProfileRequested(QString user,QString pass)
{
    emit profile(user,pass,false );
}

void loginGUI::onRegisterNewProfile()
{
    regDialog = new registerDialogGUI(this);
    regDialog->exec();
}

void loginGUI::displayError(QAbstractSocket::SocketError socketError)
{
    backtoLogin();

    switch (socketError)
    {
        case QAbstractSocket::RemoteHostClosedError:
            {
                QMessageBox::information( (QWidget*)this->parent(),tr("Remote Host Closed Connection."),
                                            tr("The connection was closed by the farside."));
                break;
            }

        case QAbstractSocket::ConnectionRefusedError:
            {
                QMessageBox::information( (QWidget*)this->parent(),tr("Unable to connect."),
                                            tr("Unable to connect."));
                break;
            }
        default:
            {
                QMessageBox::information( (QWidget*)this->parent(),tr("Network error"),
                                                tr("Unknown error occured.") );
                break;
            }
    }
}

void loginGUI::displayAuthenticationFailure()
{
    QMessageBox::information(this,tr("Authentication failure."),
                                tr("Your username,password, or both, are not registered on the server."));
}

void loginGUI::displayRegisterNewProfileFailure(bool exists)
{
    if (exists)//aka username already exists
        QMessageBox::information(this,tr("Profile registration failed."),
                                tr("your requested username is already in use, please choose another."));
    else
        QMessageBox::information(this,tr("Profile registration failed."),
                                tr("Your attempt to register a new profile was rejected, please verify that your "
                                   "password and password confirmation fields are identical."));
}

void loginGUI::displayRegisterNewProfileSuccess()
{

    QMessageBox::information(this,tr("Profile registration successful."),
                            tr("Profile creation succeeded, please proceed to log in with your new profile."));

    regDialog->close();
}

//-------------------END OF LOGINGUI------------------------------------------------

registerDialogGUI::registerDialogGUI(loginGUI *login)
{
    //Instantiations: Fields,buttons and layouts
    usernameLabel = new QLabel(tr("Username: "));
    passwordLabel = new QLabel(tr("Password: "));
    confirmPassLabel = new QLabel(tr("Confirm password: "));

    username = new QLineEdit();
    password = new QLineEdit();
    confirmpass = new QLineEdit();

    registerButton = new QPushButton(tr("Register"));
    cancelButton = new QPushButton(tr("Cancel"));

    masterLayout = new QVBoxLayout();

    fieldLayout = new QVBoxLayout();
    buttonLayout = new QHBoxLayout();

    usernameLayout = new QHBoxLayout();
    passLayout = new QHBoxLayout();
    confirmpassLayout = new QHBoxLayout();

    //Layout setup.
    usernameLayout->addStretch();
    usernameLayout->addWidget(usernameLabel);
    usernameLayout->addWidget(username);

    passLayout->addStretch();
    passLayout->addWidget(passwordLabel);
    passLayout->addWidget(password);

    confirmpassLayout->addStretch();
    confirmpassLayout->addWidget(confirmPassLabel);
    confirmpassLayout->addWidget(confirmpass);

    //Pack these fields into the fieldLayout
    fieldLayout->addLayout(usernameLayout);
    fieldLayout->addLayout(passLayout);
    fieldLayout->addLayout(confirmpassLayout);

    buttonLayout->addStretch();
    buttonLayout->addWidget(registerButton);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addStretch();

    masterLayout->addLayout(fieldLayout);
    masterLayout->addLayout(buttonLayout);
    this->setLayout(masterLayout);

    //Configuration
    password->setEchoMode(QLineEdit::Password);
    confirmpass->setEchoMode(QLineEdit::Password);
    masterLayout->setSizeConstraint(QLayout::SetFixedSize);

    //Connections
    connect(registerButton,SIGNAL(clicked()),this,SLOT(onRegister()) );
    connect(this,SIGNAL(doProfileRegistration(QString,QString)),login,SLOT(onNewProfileRequested(QString,QString)) );

    connect(cancelButton,SIGNAL(clicked()),this,SLOT(onCancel()) );
}

void registerDialogGUI::onRegister()
{
    //confirm passwords are identical.
    if (password->text() == confirmpass->text() )
    {
        emit doProfileRegistration(username->text(),password->text());
    }
    else
    {
        QMessageBox::information( (QWidget*)this->parent(),tr("Password confirmation unsuccessful."),
                                        tr("You failed to confirm your password, please try again."));
    }
}

void registerDialogGUI::onCancel()
{
    //if the user pressed the cancel button, it's equivalent to exiting the window, IE pressing the X in the upper right corner.
    //Nothing is supposed to be done, either locally or on the server if this is the case. A less verbose solution would've been ideal.
    this->close();
}

//---------------END OF REGISTERDIALOGGUI-----------------------------------------------------------

using namespace std;
int main(int argc, char *argv[])
{
    QApplication app(argc,argv);

    string SERVERIP;
    string SERVERLISTENPORT;

    if (argc == 1)//ingen argumenter utenom programmet selv
    {
        cout<<"no arguments specified, assuming localhost."<<endl;
        SERVERIP = "127.0.0.1";
        SERVERLISTENPORT ="3500";
    }
    else if (argc == 3)
    {
        cout<<"Two arguments specified, assummed to be IP::PORT. Arguments are: ";
        SERVERIP.append(argv[1] );
        SERVERLISTENPORT.append(argv[2] );
        cout<<SERVERIP<<"::"<<SERVERLISTENPORT<<endl;
    }
    else
    {
        cout<<"Error: Argument count differs from any defined protocol.\nPlease pass two arguments: "
              "The IP address and the port of the host. Alternatively, pass \"localhost\" as the argument to "
              "connect to the local machine."<<endl;
        return 0;
    }

    QString serverIP;
    quint16 port;
    try
    {
        serverIP=QString::fromStdString(SERVERIP);
        port = std::stoi(SERVERLISTENPORT);
    }
    catch (invalid_argument& e)
    {
        cerr<<"Supplied argument for port is not a parsable number. please try again"<<endl;
        return 0;
    }

    loginGUI* gui = new loginGUI(nullptr,serverIP,port);

    return app.exec();
}
