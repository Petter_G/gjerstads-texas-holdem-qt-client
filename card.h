#ifndef CARD_H
#define CARD_H

#include <string>

class card
{
    short type;
    short value;

public:
    card(short typeinput,short valueinput);
    short getValue();
    short getType();
    std::string describeType();
    std::string describeValue();
};

#endif
