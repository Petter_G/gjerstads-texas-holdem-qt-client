#include "client.h"
#include "protocolenums.h"
//#include "uipainter.h"

using namespace std;


void Client::error(const char *msg)
{
    perror(msg);
    exit(0);
}

void* Client::get_in_addr(sockaddr *sa)
{
    return &(((sockaddr_in*)sa)->sin_addr);
}


void Client::incThread(uint fd)
{
    //Select, må til for å hindre at QTs gui låser seg.
    fd_set read_fd_set;
    FD_ZERO(&read_fd_set);
    FD_SET( fd,&read_fd_set );

    struct timeval timeout;
    timeout.tv_sec=0;
    timeout.tv_usec=1000;

    int buffer[MAX_MESSAGESIZE];
    int bytecount;
    uint sockfd= fd;

    cout<<"entering prossessing loop"<<endl;
    while(true)//prossesing loop, brukes kun til innkommende meldinger fra server.
    {
        //visstnok har operativsystemets implementasjon av select en tendens til å endre timeout structen, hvorfor? Gudene vet.
        FD_ZERO(&read_fd_set);
        FD_SET(fd,&read_fd_set);
        timeout.tv_sec=0;
        timeout.tv_usec=1000;

        select(fd+1,&read_fd_set,NULL,NULL,&timeout);
        if ( !(FD_ISSET(fd,&read_fd_set)) )
            continue;

        //Vi leser først opcoden. Så analyserer vi den, og avhengig av svaret, leses relevant antall bytes fra strømmen
        vector<int> data;
        memset(&buffer,0,sizeof(buffer));
        int opcode = recv(sockfd,&opcode,sizeof(int),0);
        data.push_back(opcode);

        switch (opcode)
        {
        case REPAINT:
            {
                UI->drawTable(false);
                break;
            }
        case PLAYERSNOTREADY:
            cout<<"Unable to start game: Not all players are ready."<<endl;
            break;
        case TABLESLOTNOTIFY:
            {
                /*bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                short tableslot = buffer[0];

                if (UI->getRepsSize() == 0)
                {
                    cout<<"Player's tableslot is " <<tableslot<<endl;
                    UI->setLocalTableNr(tableslot);
                }
                else
                    cout<<"creating rep with tableslot "<<tableslot<<endl;

                UI->createRep(tableslot);*/
                break;
            }
        case INVALIDCOMMAND:
            cout<<"Invalid command."<<endl;
            break;
        case VALIDCOMMAND:
            cout<<"Valid command."<<endl;
            break;
        case NAMECHANGEDNOTIFY:
            {
                int info[2];//tableslot og messagelength
                bytecount = recv(sockfd,info,sizeof(int)*2,0 );
                bytecount = recv(sockfd,buffer,sizeof(int)*info[1],0);
                QString newname;
                newname.reserve(info[1]);

                for(int i = 0;i<info[1];i++)
                {
                    newname.push_back(buffer[i]);
                }

                string oldname=UI->getPlayerName(info[0]);
                UI->updateRepName(newname,info[0]);

                //klienten som selv sendte forespørselen trenger ikke denne erklæringen.
                if ( !(info[0] == UI->getLocalTableNr() ) )
                {
                    cout<<"\""<<oldname<< "\" is now known as: "<<UI->getPlayerName(info[0])<<endl;
                }
                break;
            }
        case HANDUPDATE:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*5,0);
                for (auto value : buffer)
                    data.push_back(value);
                UI->setHand(data);
                break;
            }
        case DELETEREP:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                short tableslot = buffer[0];
                UI->deleteRep(tableslot);
                break;
            }
        case BETUPDATE:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*3,0);
                short tableslot = buffer[0];
                int bet=buffer[1];
                int totalbet=buffer[2];
                UI->updateBet(bet,tableslot,totalbet);
                break;
            }
        case BALANCEUPDATE:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*2,0);
                short tableslot = buffer[0];
                int balance = buffer[1];
                UI->updateBalance(balance,tableslot);
                break;
            }
        case ACTIVEUPDATE:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*2,0);
                short tableslot = buffer[0];
                bool activestate = (bool)buffer[1];
                UI->updateActive(activestate,tableslot);
                break;
            }
        case ALLINUPDATE:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                short tableslot = buffer[0];
                UI->updateAllIn(tableslot);
                break;
            }
        case NAMETOLONGNOTIFY:
            {
                cout<<"Namechange invalid, name to long"<<endl;
                break;
            }
        case TABLERESET:
            {
                UI->resetTable();
                break;
            }
        case COMMUNITYCARDADDED:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*2,0);
                short cardtype = buffer[0];
                short cardvalue = buffer[1];
                UI->addCardToCommunity(cardtype,cardvalue);
                break;
            }
        case POTNOTIFY:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                UI->updatePot(buffer[0]);
                cout<<"POTNOTIFY CALLED"<<endl;
                break;
            }
        case CURRENTPLAYERNOTIFY:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                UI->updateCurrentPlayer(buffer[0]);
                break;
            }
        case CURRENTBUTTONNOTIFY:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                UI->updateCurrentButton(buffer[0]);
                break;
            }
        case MINIMUMRAISEFAILURENOTIFY:
            {
                bytecount = recv(sockfd,buffer,sizeof(int)*1,0);
                cout<<"Attention: Minimum raise is equal to the bigblind, which is "<<buffer[0]<<". Please try another action."<<endl;
                break;
            }
        case PAUPERFAILURENOTIFY:
            {
                cout<<"Attention: You cannot afford that action. Please try another action."<<endl;
                break;
            }
        case CHECKINVALIDNOTIFY:
            {
                cout<<"Attention: Checking is not possible. Please try another action."<<endl;
                break;
            }
        case NOTYOURTURNNOTIFY:
            {
                cout<<"Attention: It is not your turn."<<endl;
                break;
            }
        case REJECTCONNECTIONNOTIFY:
            {
                cout<<"Attention: Connection attempt succeeded, but was rejected due to technical limitations."<<endl;
                break;
            }
        case GAMEHALTEDNOTIFY:
            {
                cout<<"Attention: The Game is currently halted. This is due to the insufficient number of active players. \n"
                      "A minimum of 2 players is necessitated for playing Texasholdem."<<endl;
                break;
            }
        case WINNERINFONOTIFY:
            {//opcode-winnertableslot-payout-potIndex

                bytecount = recv(sockfd,buffer,sizeof(int)*3,0);
                for(int i = 0; i< 3; i++)
                {
                    data.push_back(buffer[i]);
                }
                UI->addWinnerData(data);
                break;
            }
        case CHAT:
            {
                bytecount = recv(sockfd,buffer,MAX_MESSAGESIZE+1,0);
                short tableslot = buffer[0];
                string senderName = UI->getPlayerName(tableslot);
                string message;
                message.resize(MAX_MESSAGESIZE);
                for(int i = 1; i<MAX_MESSAGESIZE;i++)
                {
                    if (buffer[i] == 0)
                        break;
                    else message[i-1]=(char)buffer[i];
                }
                cout<<"<"<<tableslot<<">"<<senderName<<": "<<message<<endl;
                break;
            }
        default:
            cout<<"error: Corrupt datastructure. opcode is "<<opcode<<endl;
            exit(0);
        }

        if (bytecount == -1)//error
        {
            cout<<"Error: Transmit error.";
            exit(0);
        }
        if (bytecount == 0)
        {
            cout<<"Error: Server terminated connection.";
            exit(0);
        }
    }
}

bool Client::inputCheck() //takk til cc.examples.com/2007/04/08/non-blocking-user-input-in-loop-without-ncurses/
{
    struct timeval tv;
    fd_set fds;
    tv.tv_sec=0;
    tv.tv_usec=1000;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO,&fds);
    select(STDIN_FILENO+1,&fds,NULL,NULL,&tv);
    return FD_ISSET(STDIN_FILENO,&fds);
}

void Client::outThread(unsigned int fd)
{
    std::string inputstring;
    cout<<"entering loop"<<endl;
    while (true)
    {
        //usleep(1000);
        if (!inputCheck() )//altså det er data i stdin
        {
            std::getline(std::cin,inputstring);
            //c++ støtter ikke switch på string.
            if (inputstring == "ready")
            {
                int command = SETREADY;
                send(fd,&command,sizeof(command),0);
            }
            else if (inputstring == "not ready")
            {
                int command = CLEARREADY;
                send(fd,&command,sizeof(command),0);
            }
            /*else if (inputstring=="startgame")
            {
                int command = STARTGAME;
                send(fd,&command,sizeof(command),0);
            }*/
            else if (inputstring.substr(0,5)=="name ")
            {
                string name= inputstring.substr(5);
                int dataparcel[name.size()+2];
                for (uint i = 0;i<name.size()+1;i++)
                {
                    dataparcel[i+1] = (int)name[i];
                }
                dataparcel[0] = SETNAME;
                dataparcel[sizeof(dataparcel)/sizeof(int)] = 0;
                send(fd,&dataparcel,sizeof(dataparcel),0);
            }
            else if (inputstring=="check")
            {
                int command = CHECK;
                send(fd,&command,sizeof(command),0);
            }
            else if (inputstring=="call")
            {
                int command = CALL;
                send(fd,&command,sizeof(command),0);
            }
            else if (inputstring=="fold")
            {
                int command = FOLD;
                send(fd,&command,sizeof(command),0);
            }
            else if (inputstring=="allin")
            {
                int command= ALLIN;
                send(fd,&command,sizeof(command),0);
            }
            else if (inputstring.substr(0,6)=="raise ")
            {
                try
                {
                    int dataparcel[2];
                    dataparcel[0]= RAISE;
                    dataparcel[1]= stoi(inputstring.substr( 6,5) );
                    send(fd,&dataparcel,sizeof(dataparcel),0);
                }
                catch (invalid_argument& e)
                {
                    cout<<"Error: Argument has to be a positive integer."<<endl;
                }
            }
            else if (inputstring.substr(0,9) == "transfer ")
            {
                try
                {
                    int dataparcel[2];
                    dataparcel[0]=TRANSFER;
                    dataparcel[1]=stoi(inputstring.substr( 9,5));
                    send(fd,&dataparcel,sizeof(dataparcel),0);
                }
                catch(invalid_argument& e)
                {
                    cout<<"Error: Argument has to be a positive integer."<<endl;
                }
            }
            else if (inputstring.substr(0,7) == "refill ")
            {
                try
                {
                    int dataparcel[2];
                    dataparcel[0]=REFILL;
                    dataparcel[1]=stoi(inputstring.substr( 7,5));
                    send(fd,&dataparcel,sizeof(dataparcel),0);
                }
                catch(invalid_argument& e)
                {
                    cout<<"Error: Argument has to be a positive integer."<<endl;
                }
            }
            else if (inputstring=="repaint")
            {
                UI->drawTable(false);
            }
            else if (inputstring =="help")
            {
                UI->drawTable(true);
            }
            else if (inputstring=="exit")
            {
                exit(0);
            }
            else if (inputstring=="cheat reset")//cheat
            {
                int data = RESET;
                send(fd,&data,sizeof(data),0);
            }
            else //chat
            {
                if (inputstring.size() > MAX_MESSAGESIZE)
                {
                    cout<<"Error: Message to long"<<endl;
                    continue;
                }
                if (inputstring.size() == 0)
                    continue;
                int dataparcel[inputstring.size()+1];
                dataparcel[0]=CHAT;
                for (uint i = 0; i<inputstring.size();i++)
                {
                    dataparcel[i+1] = inputstring[i];
                }
                send(fd,&dataparcel,sizeof(dataparcel),0);
            }
        }
    }
}

Client::Client(string SERVERLISTENPORT,string SERVERIP,HoldemGUI* gui)
{

    int sockfd;
    addrinfo hints,*servinfo;

    memset(&hints,0,sizeof(hints) );
    hints.ai_family= AF_INET;
    hints.ai_socktype=SOCK_STREAM;
    int status = getaddrinfo(SERVERIP.c_str(),SERVERLISTENPORT.c_str(),&hints,&servinfo);
    if (status != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
    }

    sockfd=socket(servinfo->ai_family,servinfo->ai_socktype,servinfo->ai_protocol);
    if (sockfd ==-1)
        cout<<"could not create socket"<<endl;

    status = connect(sockfd,servinfo->ai_addr,servinfo->ai_addrlen);
    if (status == -1)
    {
        cout<<"error connecting"<<endl;
        exit(0);
    }

    char s[255];
    inet_ntop(servinfo->ai_family,get_in_addr((sockaddr *)servinfo->ai_addr),s,sizeof(s));
    freeaddrinfo(servinfo);

    //UI må opprettes
    UI = std::unique_ptr<UIpainter>(new UIpainter );
    //UI_QT = new UIpainter_main();
    //UI_QT->show();

    //NOTE:Avviker fra global bruk, som var den originale planen, gir veldig dårlig feilmelding. Spørs om vi kan takke Stroustrup for det.
    //Moral of the story: functorer må være del av en klasse.
    std::thread t1(&Client::incThread,this,sockfd );
    std::thread t2(&Client::outThread,this,sockfd );
    t1.join();
    t2.join();
    close(sockfd);
}
