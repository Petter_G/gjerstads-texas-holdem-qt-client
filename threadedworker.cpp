#include "threadedworker.h"
#include "protocolenums.h"
#include "holdemgui.h"

#include <QMessageBox>
#include <QString>

#include <string>
#include <iostream>

using namespace std;

worker::worker(loginGUI* log, QString serverIP, quint16 serverport, HoldemGUI* Holdem, loungeGUI *Lounge)
            : login(log),serverIP(serverIP),port(serverport),holdem(Holdem),lounge(Lounge)
{
    //av magiske årsaker må dette gjøres. Gudene vet kanskje hvorfor dette ikke er gjort ved default.
    qRegisterMetaType<QAbstractSocket::SocketError*>("QAbstractSocket::SocketError");

    //Mer forståelig at denne må registreres, mulig digia mente at man skal bruke QVector.
    qRegisterMetaType<vecUint>("vecUint");

    socket = new QTcpSocket(this);
    painter = new UIpainter;

    //lokale koblinger
    connect(socket,SIGNAL(readyRead()),this,SLOT(incomingData()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnectFromServerHost()) );

    //koblinger mot loginGUI
    connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),login, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(this,SIGNAL(authenticationFailure() ),login,SLOT(displayAuthenticationFailure()));
    connect(this,SIGNAL(registerNewProfileFailure(bool) ), login,SLOT(displayRegisterNewProfileFailure(bool) ));

    connect(this,SIGNAL(registerNewProfileSuccess()),login,SLOT(displayRegisterNewProfileSuccess()) );
    connect(this,SIGNAL(switch_to_loginGUI()),login,SLOT(backtoLogin()) );
    connect(this,SIGNAL(switch_to_loungeGUI(uint)),login,SLOT(loginSuccess(uint)) );
    connect(this,SIGNAL(switch_to_holdemGUI() ),login,SLOT(joinSuccess()) );
    connect(this,SIGNAL(switch_to_loungeGUI() ),login,SLOT(backToLounge()) );

    //hvis noe skjer, slik at switch_to_loginGUI kalles, må de to øvrige guiene renses slik at de ikke har gamle data hvis brukeren
    //på ny kobler til.
    connect(this,SIGNAL(signalGUI_resetYourself()),holdem,SLOT(clearGUI()) );
    connect(this,SIGNAL(signalGUI_resetYourself()),lounge,SLOT(clearGUI()) );

    //siden holdemGUI fortsatt er usynlig, og dermed er brukerinput umulig, kan vi gjøre ett direkte funksjonskall uten frykt for å
    //lage krøll med usynkroniserte data, ikke noe signals and slots.
    holdem->hereIam(this);
    lounge->hereIam(this);

    //koblinger til holdemGUIs mange receivers.
    connect(this,SIGNAL(appendHoldemChatArea(vecUint,QString)),holdem,SLOT(chatReceiver(vecUint,QString)) );
    connect(this,SIGNAL(genericHoldemSignal(vecUint,QString,uint,QString)),holdem,SLOT(genericReceiver(vecUint,QString,uint,QString)));
    connect(this,SIGNAL(genericHoldemSignal(uint,uint,uint,uint)),holdem,SLOT(genericReceiver(uint,uint,uint,uint)) );
    connect(this,SIGNAL(genericHoldemSignal(uint,uint,uint)),holdem,SLOT(genericReceiver(uint,uint,uint)) );
    connect(this,SIGNAL(genericHoldemSignal(uint,uint)),holdem,SLOT(genericReceiver(uint,uint)) );
    connect(this,SIGNAL(genericHoldemSignal(uint)),holdem,SLOT(genericReceiver(uint)));

    //koblinger til lounges receivers
    connect(this,SIGNAL(appendLoungeChatArea(vecUint,int)),lounge,SLOT(chatReceiver(vecUint,int)) );
    connect(this,SIGNAL(genericLoungeSignal(vecUint,QString,uint,vecUint)),lounge,SLOT(genericReceiver(vecUint,QString,uint,vecUint)));
    connect(this,SIGNAL(genericLoungeSignal(uint,uint,uint,uint)),lounge,SLOT(genericReceiver(uint,uint,uint,uint)) );
    connect(this,SIGNAL(genericLoungeSignal(uint,uint,uint)),lounge,SLOT(genericReceiver(uint,uint,uint)) );
    connect(this,SIGNAL(genericLoungeSignal(uint,uint)),lounge,SLOT(genericReceiver(uint,uint)) );
    connect(this,SIGNAL(genericLoungeSignal(uint)),lounge,SLOT(genericReceiver(uint)));

}

worker::~worker()
{
    socket->close();
    delete socket;
    delete painter;
}

const QTcpSocket* worker::getSocket()
{
    return socket;
}

UIpainter* worker::getpainter()
{
    return painter;
}

vector<uint> worker::BlockReader(uint blockCount, QDataStream& reader)
{
    //std::cout<<"bytes available: "<<socket->bytesAvailable()<<std::endl;
    vector<uint> blocks;

    while ( socket->bytesAvailable() < (blockCount * 4) );//busy waiting, ikke helt optimalt.

    for (uint i = 0; i< blockCount;i++)
    {
        uint value = 0;
        reader >> value;
        blocks.push_back(value);
    }
    //std::cout<<"bytes at end: "<<socket->bytesAvailable()<<std::endl;
    return blocks;
}

void worker::incomingData()
{
    while (socket->bytesAvailable() != 0)
    {
        //std::cout<<"threadID in incomingData is: "<<std::this_thread::get_id()<<std::endl;
        QDataStream reader(socket);
        reader.setByteOrder(QDataStream::LittleEndian);
        quint64 available = socket->bytesAvailable();
        //clog<<"available data at start of incoming "<<available<<endl;

        quint32 opcode;
        reader >> opcode;
        clog<<"opcode is "<<opcode<<endl;
        switch (opcode)//ny implementasjon av tilsvarende switch i client, tilpasset det grafiske grensesnittet.
        {
            case PLAYERSTATECHANGEDNOTIFY:
            {
                //opcode-serversidefd-gameid-dcstate
                uint serversidefd;
                uint gameid;
                uint dcstate;
                reader >> serversidefd;
                reader >> gameid;
                reader >> dcstate;

                emit genericLoungeSignal(opcode,serversidefd,gameid,dcstate);
                break;
            }
            case PLAYERLEFTLOCALROOMNOTIFY:
            {
                uint tableslot;
                reader >> tableslot;

                emit genericHoldemSignal(PLAYERLEFTLOCALROOMNOTIFY,tableslot);
                break;
            }
            case PLAYERJOINEDROOMNOTIFY:
            {
                //opcode-serversidefd-gameid
                uint serversidefd;
                uint gameid;

                reader >> serversidefd;
                reader >> gameid;
                emit genericLoungeSignal(PLAYERJOINEDROOMNOTIFY,serversidefd,gameid);
                break;
            }
            case JOINFAILURENOTIFY:
            {
                emit genericLoungeSignal(opcode);
                break;
            }
            case JOINSUCCESSNOTIFY:
            {
                emit switch_to_holdemGUI();
                break;
            }
            /*case CLIENTDISSCONNECTEDNOTIFY:
            {
                //opcode-serversidefd-gameid
                uint serversidefd;
                uint gameid;
                reader >> serversidefd;
                reader >> gameid;

                emit genericLoungeSignal(CLIENTDISSCONNECTEDNOTIFY,serversidefd,gameid);
                break;
            }*/
            case NEWPLAYERNOTIFY:
            {//opcode-gameid-serversidefd-messagelength-character(1-20)
                uint gameid;
                uint serversidefd;
                uint messagelength;
                reader >> gameid;
                reader >> serversidefd;
                reader >> messagelength;

                vecUint data = BlockReader(messagelength,reader);
                vecUint extra;

                extra.push_back(gameid);
                extra.push_back(serversidefd);
                emit genericLoungeSignal(data,NULL,NEWPLAYERNOTIFY,extra);
                break;
            }
            case NEWGAMENOTIFY:
            {
                uint id;
                reader>>id;

                emit genericLoungeSignal(NEWGAMENOTIFY,id);
                break;
            }
            case AUTHENTICATIONSUCCESSNOTIFY:
            {
                clog<<"authentication success."<<endl;
                uint cookie;
                reader>>cookie;

                emit switch_to_loungeGUI(cookie);
                break;
            }
            case AUTHENTICATIONFAILURENOTIFY:
            {
                clog<<"authentication failure."<<endl;
                emit authenticationFailure();
                break;
            }
            /*case AUTHENTICATEREQUIREDNOTIFY:
            {
                //mulig denne er overflødig, lar den ligge enn så lenge
                break;
            }*/
            case INVALIDPROFILECREATION:
            {
                uint exists;
                reader>>exists;
                if (exists == 1)
                    clog<<"profile creation failed, username already in use."<<endl;
                else
                    clog<<"profile creation failure"<<endl;
                emit registerNewProfileFailure(exists);
                break;
            }
            case VALIDPROFILECREATION:
            {
                clog<<"profile creation successful"<<endl;
                emit registerNewProfileSuccess();
                break;
            }
            case SERVERSHUTDOWNNOTIFY:
            {
                emit genericHoldemSignal(SERVERSHUTDOWNNOTIFY);
                break;
            }
            case REPAINT:
            {
                //I utgangspunktet(dvs før grafisk GUI) ble servers repaint signal brukt til å instruere klientene om å tegne på nytt.
                //Dette fordi det tekstbaserte grensesnittet var terminalen, ikke noe fancy rammeverk var i bruk.
                //Nå med qt GUI, bortfaller hele den gamle problemstillingen. Men en gjenstår: visning av winnerdata.
                //vi kan gjenbruke repaint signalet til å fortelle GUIet at server nå ikke sender ut flere winnerData objekter.
                emit genericHoldemSignal(REPAINT);
                break;
            }
            case PLAYERSNOTREADY:
            {
                emit genericHoldemSignal(PLAYERSNOTREADY);
                break;
            }
            case TABLESLOTNOTIFY:
            {
                uint tableslot;
                uint trulyNew;
                uint local;

                reader>> tableslot;
                reader>> trulyNew;
                reader>> local;
                clog<<"tableslotnotify number is"<<tableslot<<endl;
                bool first = painter->createRep(tableslot);//obsolete

                emit genericHoldemSignal(TABLESLOTNOTIFY,tableslot,local,trulyNew);
                break;
            }
            case INVALIDCOMMAND:
            {
                emit genericHoldemSignal(INVALIDCOMMAND);
                break;
            }
            case VALIDCOMMAND:
            {
                emit genericHoldemSignal(VALIDCOMMAND);
                break;
            }
            case NAMECHANGEDNOTIFY:
            {//messy
            //opcode-serversidefd-silent-tableslot-messagelength-character(1-20)
                vecUint info = BlockReader(FOUR_BLOCKS,reader);
                vecUint data = BlockReader(info[3],reader);

                QString QnewName;
                for(int i = 0; i< info[3];i++)
                {
                    QnewName.append((char)data[i]);
                }
                //uansett skal samtlige klienter få en oppdatering slik at deres lounge er korrekt.
                emit genericLoungeSignal(info,QnewName,NAMECHANGEDNOTIFY,data);
                break;
            }
            case TABLESLOTCHANGEDNAMENOTIFY://opcode-tableslot-silent-namelength-character(1-20)
            {
                vecUint info = BlockReader(THREE_BLOCKS,reader);

                QString QnewName;
                vecUint data = BlockReader(info[2],reader);

                for(auto& character : data)
                {
                    QnewName.append((char)character);
                }

                //før vi oppdaterer, hent ut det gamle navnet, det må brukes slik at statusoppdateringen i holdem gir mening.
                QString QoldName = QString::fromStdString( painter->getPlayerName(info[0]) );

                //oppdater gjeldende reps, og så send ut et signal.
                painter->updateRepName(QnewName,info[0]);
                emit genericHoldemSignal(info,QoldName,TABLESLOTCHANGEDNAMENOTIFY,QnewName);
                break;
            }
            case NAMECHANGEINVALIDNOTIFY:
            {
                emit genericLoungeSignal(NAMECHANGEINVALIDNOTIFY);
                emit genericHoldemSignal(NAMECHANGEINVALIDNOTIFY);
                break;
            }
            case HANDUPDATE:
            {
                vecUint data = BlockReader(FIVE_BLOCKS,reader);
                painter->setHand(data);
                ushort localTableNr = painter->getLocalTableNr();
                if (data[0] == localTableNr)
                    emit genericHoldemSignal(data,nullptr,HANDUPDATE,nullptr);
                break;
            }
            case DELETEREP:
            {
                uint tableslot;
                reader>>tableslot;
                painter->deleteRep(tableslot);
                emit genericHoldemSignal(DELETEREP,tableslot);
                break;
            }
            case BETUPDATE:
            {
                uint tableslot;
                reader>>tableslot;
                uint bet;
                reader>>bet;
                uint totalbet;
                reader>>totalbet;
                painter->updateBet(tableslot,bet,totalbet);
                emit genericHoldemSignal(BETUPDATE,tableslot,bet,totalbet);
                break;
            }
            case BALANCEUPDATE:
            {
                uint tableslot;
                uint balance;
                reader>>tableslot;
                reader>>balance;
                painter->updateBalance(balance,tableslot);
                emit genericHoldemSignal(BALANCEUPDATE,tableslot,balance);
                break;
            }
            case ACTIVEUPDATE:
            {
                uint tableslot;
                uint activestate;
                reader>>tableslot;
                reader>>activestate;
                painter->updateActive((bool)activestate,tableslot);
                emit genericHoldemSignal(ACTIVEUPDATE,tableslot,activestate);
                break;
            }
            case ALLINUPDATE:
            {
                uint tableslot;
                reader>>tableslot;
                painter->updateAllIn(tableslot);
                emit genericHoldemSignal(ALLINUPDATE,tableslot);
                break;
            }
            case NAMETOLONGNOTIFY:
            {
                emit genericHoldemSignal(NAMETOLONGNOTIFY);
                break;
            }
            case TABLERESET:
            {
                painter->resetTable();
                emit genericHoldemSignal(TABLERESET);
                break;
            }
            case COMMUNITYCARDADDED:
            {
                uint cardtype;
                uint cardvalue;

                reader>>cardtype;
                reader>>cardvalue;

                painter->addCardToCommunity(cardtype,cardvalue);
                emit genericHoldemSignal(COMMUNITYCARDADDED,cardtype,cardvalue);
                break;
            }
            case POTNOTIFY:
            {
                uint pot;
                reader >> pot;
                painter->updatePot(pot);
                emit genericHoldemSignal(POTNOTIFY,pot);
                break;
            }
            case CURRENTPLAYERNOTIFY:
            {
                uint oldCurrentPlayer = painter->getCurrentPlayerTableNr();
                uint newcurrentPlayer;
                reader >>newcurrentPlayer;
                painter->updateCurrentPlayer(newcurrentPlayer);
                emit genericHoldemSignal(CURRENTPLAYERNOTIFY,newcurrentPlayer,oldCurrentPlayer);
                break;
            }
            case CURRENTBUTTONNOTIFY:
            {
                uint currentButton;
                reader >>currentButton;
                painter->updateCurrentButton(currentButton);
                emit genericHoldemSignal(CURRENTBUTTONNOTIFY,currentButton);
                break;
            }
            case MINIMUMRAISEFAILURENOTIFY:
            {
                uint bigblind;
                reader >> bigblind;
                clog<<"bigBlind is "<<bigblind<<endl;
                emit genericHoldemSignal(MINIMUMRAISEFAILURENOTIFY,bigblind);
                break;
            }
            case PAUPERFAILURENOTIFY:
            {
                emit genericHoldemSignal(PAUPERFAILURENOTIFY);
                break;
            }
            case CHECKINVALIDNOTIFY:
            {
                emit genericHoldemSignal(CHECKINVALIDNOTIFY);
                break;
            }
            case NOTYOURTURNNOTIFY:
            {
                emit genericHoldemSignal(NOTYOURTURNNOTIFY);
                break;
            }
            case REJECTCONNECTIONNOTIFY:
            {
                //obsolete?
                //emit genericSignal(REJECTCONNECTIONNOTIFY);
                break;
            }
            case GAMEHALTEDNOTIFY:
            {
                emit genericHoldemSignal(GAMEHALTEDNOTIFY);
                break;
            }
            case GAMEPAUSEDNOTIFY:
            {
                emit genericHoldemSignal(GAMEPAUSEDNOTIFY);
                break;
            }
            case WINNERINFONOTIFY:
            {
                vecUint data = BlockReader(THREE_BLOCKS,reader);
                vector<int> formattedData;
                formattedData.push_back(WINNERINFONOTIFY);
                for(auto& value : data)
                {
                    formattedData.push_back(value);
                }
                painter->addWinnerData(formattedData);
                break;
            }
            case CHAT:
            {
                int tableslot;
                int length;
                int fd;

                reader>> tableslot;
                reader>> fd;
                reader>> length;

                vecUint data = BlockReader(length,reader);
                if (tableslot == -1)//lounger
                {
                    emit appendLoungeChatArea(data,fd);
                }
                else
                {
                    QString name = QString::fromStdString(painter->getPlayerName(tableslot));
                    emit appendHoldemChatArea(data,name);
                }
                break;
            }
            default:
            {
                int error;
                cerr<<"Error: pipe corrupted, dumping raw data(by blocks of 4 bytes):"<<endl;
                while(socket->bytesAvailable() != 0)
                {
                    reader>>error;
                    cerr<<error<<" "<<endl;
                }
                cerr<<endl;
                break;
            }
        }
    }
}

// slots

/*void worker::onConnected()
{

}*/

void worker::connectToServer()
{
    socket->connectToHost(serverIP,port);
}

void worker::disconnectFromServerHost()
{
    socket->disconnectFromHost();

    //we've disconnected from the servers host and will return to the login screen(loginGUI),
    //we will need to clean the GUI so that old data won't be present if we reconnect to the server.
    cout<<"DISCONNECTFROMSERVERHOST CALLED()"<<endl;
    emit signalGUI_resetYourself();
    painter->resetObject();
    emit switch_to_loginGUI();
}

void worker::writeMessageToSocket(QString text)
{
    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    //kontruer en byterray bestående av inter, send så denne.
    uint opcode = SHORTCHAT;
    parcelMaker<<opcode;

    for(auto& letter : text.toStdString() )
    {
        parcelMaker<<(uint)letter;
    }
    socket->write(parcel);
}

void worker::writeDisplayNameChangedToSocket(QString name)
{
    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    uint opcode = SETNAME;
    parcelMaker<<opcode;

    for(auto& letter : name.toStdString())
    {
        parcelMaker<<(uint)letter;
    }
    socket->write(parcel);
}

void worker::genericCommandReceiver(uint opcode)
{
    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    switch(opcode)
    {
        /*case STARTGAME:
        {
            parcelMaker<<opcode;
            socket->write(parcel);
            break;
        }*/
        case CALL:
        {
            parcelMaker<<opcode;
            socket->write(parcel);
            break;
        }
        case ALLIN:
        {
            parcelMaker<<opcode;
            socket->write(parcel);
            break;
        }
        case FOLD:
        {
            parcelMaker<<opcode;
            socket->write(parcel);
            break;
        }
        case CREATE:
        {
            parcelMaker<<opcode;
            socket->write(parcel);
            break;
        }
        case LEAVEROOM:
        {
            parcelMaker<<opcode;
            socket->write(parcel);

            emit switch_to_loungeGUI();
            break;
        }
        default:
        {
            cout<<"error: opcode("<<opcode<<") unknown."<<endl;
        }

    }
}

void worker::genericCommandReceiver(uint opcode, uint value)//brukes kun av raise
{
    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    switch (opcode)
    {
        case RAISE:
        {
            uint raisevalue= value;
            parcelMaker<<opcode;
            parcelMaker<<raisevalue;
            socket->write(parcel);
            break;
        }
        case REFILL:
        {
            parcelMaker<<opcode;
            parcelMaker<<value;
            socket->write(parcel);
            break;
        }
        case TRANSFER:
        {
            parcelMaker<<opcode;
            parcelMaker<<value;
            socket->write(parcel);
            break;
        }
        default:
        {
            cout<<"error: opcode("<<opcode<<") unknown."<<endl;
        }
    }
}

void worker::genericCommandReceiver(uint opcode, uint value, uint value2)
{
    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    switch(opcode)
    {
        case JOIN:
        {
            parcelMaker<<opcode;
            parcelMaker<<value;
            parcelMaker<<value2;
            socket->write(parcel);
            break;
        }
        default:
        {
            cout<<"error: opcode("<<opcode<<") unknown."<<endl;
        }
    }
}

void worker::doProfile(QString username, QString password,bool login)
{
    //doProfile is the only function that can be called before a connection has been established,
    //whether we succeed our first login/register attempt, we create a lasting connection none the less.
    //we'll have to connect the socket if it isn't already connected
    if (socket->state() != QTcpSocket::ConnectedState )
        connectToServer();

    QByteArray parcel;
    QDataStream parcelMaker(&parcel,QIODevice::WriteOnly);
    parcelMaker.setByteOrder(QDataStream::LittleEndian);

    if (login)
    {
        clog<<"login attempt commencing"<<endl;
        parcelMaker<<LOGIN;
    }
    else
    {
        clog<<"register new profile attempt commencing"<<endl;
        parcelMaker<<REGISTER;
    }

    parcelMaker<<username.size();
    parcelMaker<<password.size();

    for(auto& letter : username.toStdString())
    {
        parcelMaker<<(uint)letter;
    }

    for(auto& letter : password.toStdString())
    {
        parcelMaker<<(uint)letter;
    }

    socket->write(parcel);

}
